<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swensonhe
 */
if (is_home()) {
    $thePost = get_post(get_option('page_for_posts'));
} else {
    $thePost = $post;
}
?>
<?php if (((rwmb_meta('form_select', [], $thePost->ID) !== '') && rwmb_meta('form_select', [], $thePost->ID) !== 'none') || (is_post_type_archive('work') || is_tax('work_type'))){ ?>
<?php if (rwmb_meta('form_select', [], $thePost->ID) === 'contact-palm' || rwmb_meta('form_select', [], $thePost->ID) === 'contact-palm-w/o-title'): ?>
<footer class="container-fluid" id="contact"
        style='background-image: url("<?php bloginfo('template_url'); ?>/assets/palm-trees.jpg"); background-position: center; background-size: cover;'>
    <?php if (rwmb_meta('form_select', [], $thePost->ID) === 'contact-palm'): ?>
        <h1 class="text-center homepage-section-header white">We are in Los Angeles' Silicon Beach.<br> Get in touch with our experts to discuss your<br>technological needs.</h1>
    <?php endif; ?>
    <?php elseif (rwmb_meta('form_select', [], $thePost->ID) === 'contact-white'): ?>
    <footer class="container-fluid" id="contact">
        <?php endif; ?>
        <div class="container">
            <div class="row">
                <div class="contact-footer-block <?php if (rwmb_meta('form_select', [], $thePost->ID) !== 'contact-white') {
                    echo "contact-footer-block--margin-top";
                } ?>">
                    <div class="hidden-xs">
                        <h1>We always welcome new partnerships.</h1>
                        <p>Give us a few details about your company and we’ll have someone<br>contact you within 15 minutes (in normal business hours).</p>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if(rwmb_meta('footer_shortcode')){
                                    echo do_shortcode(rwmb_meta('footer_shortcode'));
                                } else{
                                    echo do_shortcode((get_option('swenson_he_options')['contact_form']));
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="visible-xs-block">
                        
                        <h1>We always welcome new partnerships.</h1>
                        <a class="contact-footer-block__call text-center"
                           href="tel:<?php echo get_option('swenson_he_options')['phone'] ?>">call us</a>
                        <p>Give us a few details about your company and we’ll have someone<br>contact you within 15 minutes (in normal business hours).</p>
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                if(rwmb_meta('footer_shortcode')){
                                    echo do_shortcode(rwmb_meta('footer_shortcode'));
                                } else{
                                    echo do_shortcode((get_option('swenson_he_options')['contact_form']));
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer><!-- #colophon -->

    
    
    <?php } ?>
    <?php wp_footer(); ?>
<script>
jQuery(document).ready(function(){
    jQuery('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        jQuery('#contactfilename').html(fileName);
    });
});    
</script>
    </body>
    </html>
