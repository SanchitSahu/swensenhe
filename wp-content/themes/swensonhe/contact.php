<?php
/* Template Name: Contact */

get_header(); ?>
    <main>
        <!--Start Contact form-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12 contact-form-col">
                    <div class="contact-block contact-footer-block contact-block--form">
                        <div class="row">
                            <div class="col-md-12">
                                <h1>We always welcome new partnerships.</h1>
                                <a class="contact-footer-block__call text-center visible-xs-block"
                                    href="tel:<?php echo get_option('swenson_he_options')['phone'] ?>">call us</a>
                                <p>
                                    Give us a few details about your company and we’ll have someone contact you within 15 minutes (in normal business hours).
                                </p>

                            </div>
                            <div class="col-md-12">
                                <?php
                                echo do_shortcode((get_option('swenson_he_options')['contact_form']));
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End Contact Form-->
        
        <!--Start change design address part and dynamic address part-->
        
        <div class="container-fluid contact-picture-container">
            <div class="row contact-picture-container__row">
                <picture>
                    <source media="(min-width: 1920px)"
                            srcset="<?php bloginfo('template_url'); ?>/assets/beach-xlg.jpg">
                    <source media="(min-width: 1366px)" srcset="<?php bloginfo('template_url'); ?>/assets/beach-md.jpg">
                    <source media="(min-width: 1024px)" srcset="<?php bloginfo('template_url'); ?>/assets/beach-md.jpg">
                    <source media="(min-width: 768px)" srcset="<?php bloginfo('template_url'); ?>/assets/beach-sm.jpg">
                    <img class="contact-picture" src="<?php bloginfo('template_url'); ?>/assets/beach-sm.jpg">
                </picture>
            </div>
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-4 col-md-7 col-md-offset-5 contact-info-col">
                            <div class="contact-block contact-block--info">
                                <h1><?php if(get_field('address_title')) { echo get_field('address_title');}?></h1>
                                <h5>Contact</h5>
                                <p><?php if(get_field('email')) { echo get_field('email');}?>
                                    <br>
                                    <?php if(get_field('phone')) { echo get_field('phone');}?>
                                </p>
                                <div class="row contact-block__icons-row">
                                    <div class="col-md-7">
                                        <h5>Location</h5>
                                        <?php if(get_field('location')) { echo get_field('location');}?>
                                    </div>
                                    <div class="col-md-5" style="padding-right: 0px;">
                                        <div class="contact-block__icons">
                                            <a href="<?php echo get_option('swenson_he_options')['facebook_link'] ?>">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                                                    <path fill="#B1B1B1" fill-rule="evenodd"
                                                          d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm18.205 9.429V17.18h2.553l.339-3.187h-2.892l.005-1.595c0-.832.079-1.277 1.273-1.277h1.596V7.933h-2.554c-3.067 0-4.146 1.546-4.146 4.147v1.913h-1.912v3.187h1.912v9.249h3.826z"/>
                                                </svg>
                                            </a>
                                            <a href="<?php echo get_option('swenson_he_options')['instagram_link'] ?>">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                                                    <g fill="#B1B1B1" fill-rule="evenodd">
                                                        <path d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm17-9.067c-2.462 0-2.77.011-3.738.055-.965.044-1.624.197-2.2.421a4.44 4.44 0 0 0-1.606 1.046 4.45 4.45 0 0 0-1.047 1.606c-.225.576-.378 1.236-.421 2.2-.043.967-.055 1.277-.055 3.74 0 2.461.011 2.77.055 3.737.045.965.197 1.624.421 2.2a4.443 4.443 0 0 0 1.046 1.606 4.44 4.44 0 0 0 1.606 1.047c.576.224 1.236.377 2.2.421.968.044 1.277.055 3.739.055 2.462 0 2.77-.01 3.737-.055.966-.044 1.625-.197 2.202-.421a4.436 4.436 0 0 0 1.605-1.047 4.45 4.45 0 0 0 1.047-1.606c.223-.576.376-1.235.42-2.2.044-.967.056-1.276.056-3.738 0-2.462-.012-2.771-.055-3.738-.045-.966-.198-1.625-.421-2.201a4.45 4.45 0 0 0-1.047-1.606 4.431 4.431 0 0 0-1.605-1.046c-.578-.224-1.238-.377-2.203-.42-.967-.045-1.275-.056-3.738-.056h.003z"/>
                                                        <path d="M16.187 9.567h.814c2.42 0 2.708.009 3.664.052.884.04 1.363.189 1.683.313.423.164.725.36 1.042.678.317.317.514.62.678 1.042.124.32.272.8.313 1.683.043.956.053 1.243.053 3.663s-.01 2.707-.053 3.663c-.04.884-.189 1.363-.313 1.683a2.803 2.803 0 0 1-.678 1.041 2.804 2.804 0 0 1-1.042.678c-.32.125-.8.272-1.683.313-.956.043-1.243.053-3.664.053-2.421 0-2.708-.01-3.664-.053-.884-.041-1.364-.189-1.683-.313a2.808 2.808 0 0 1-1.043-.678 2.81 2.81 0 0 1-.679-1.042c-.124-.32-.272-.8-.312-1.683-.043-.956-.052-1.243-.052-3.664 0-2.421.009-2.707.052-3.663.04-.884.188-1.364.312-1.683a2.81 2.81 0 0 1 .679-1.043c.317-.317.62-.514 1.043-.678.32-.125.8-.272 1.683-.313.837-.038 1.16-.05 2.85-.051v.002zm5.654 1.506a1.088 1.088 0 1 0 0 2.176 1.088 1.088 0 0 0 0-2.177zM17 12.343a4.656 4.656 0 1 0 0 9.314 4.656 4.656 0 0 0 0-9.313z"/>
                                                        <path d="M17 13.978a3.022 3.022 0 1 1 0 6.044 3.022 3.022 0 0 1 0-6.044z"/>
                                                    </g>
                                                </svg>
                                            </a>
                                            <a href="<?php echo get_option('swenson_he_options')['linkedin_link'] ?>">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                                                    <path fill="#B1B1B1" fill-rule="evenodd"
                                                          d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm12.014-2.92H8.16v11.576h3.853V14.08zm.253-3.58c-.025-1.136-.836-2-2.154-2s-2.18.864-2.18 2c0 1.11.836 2 2.13 2h.025c1.343 0 2.18-.89 2.18-2zm13.64 8.519c0-3.556-1.9-5.21-4.435-5.21-2.046 0-2.961 1.123-3.473 1.911v-1.64h-3.853c.051 1.087 0 11.576 0 11.576H18V19.19c0-.346.025-.69.127-.939.279-.69.913-1.406 1.977-1.406 1.395 0 1.952 1.061 1.952 2.617v6.193h3.852v-6.637z"/>
                                                </svg>
                                            </a>
                                            <a href="<?php echo get_option('swenson_he_options')['twitter_link'] ?>">
                                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                                                    <path fill="#B1B1B1" fill-rule="evenodd"
                                                          d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm16.49-3.182l.037.588-.595-.072c-2.164-.276-4.055-1.212-5.66-2.785l-.785-.78-.202.576c-.428 1.284-.155 2.64.737 3.553.476.504.369.576-.452.276-.285-.096-.535-.168-.559-.132-.083.084.202 1.177.428 1.609.31.6.94 1.188 1.63 1.537l.582.276-.69.012c-.665 0-.69.012-.618.264.238.78 1.177 1.608 2.224 1.969l.737.252-.642.384a6.695 6.695 0 0 1-3.187.888c-.535.012-.975.06-.975.096 0 .12 1.45.793 2.295 1.057 2.533.78 5.541.444 7.8-.889 1.606-.948 3.211-2.833 3.96-4.658.405-.972.809-2.749.809-3.601 0-.552.036-.624.702-1.285.392-.384.76-.804.832-.924.119-.228.107-.228-.5-.024-1.01.36-1.153.312-.654-.228.37-.384.81-1.08.81-1.285 0-.036-.18.024-.381.133-.215.12-.69.3-1.047.408l-.642.204-.583-.396c-.32-.216-.773-.457-1.01-.529-.607-.168-1.534-.144-2.081.048-1.487.54-2.426 1.933-2.32 3.458z"/>
                                                </svg>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End change design address part and dynamic address part-->
    </main>
<?php
get_footer();
