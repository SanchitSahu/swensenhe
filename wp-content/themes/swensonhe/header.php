<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package swensonhe
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <meta property="og:image" content="https://s3-us-west-1.amazonaws.com/swensonhe-website/sharing_image.jpg">
    <?php wp_enqueue_style('load-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css'); ?>
    <?php wp_head(); ?>
</head>

<?php if (is_front_page() || $post->post_name == 'services'): ?>
<body <?php body_class('slide--js background-black'); ?> data-section-name="home">
<?php else: ?>
<body>
<?php endif; ?>


<?php
if (is_home()) {
    $thePost = get_post(get_option('page_for_posts'));
} else {
    $thePost = $post;
}
if (rwmb_meta('header_options', [], $thePost->ID) === 'black'): ?>
<header class="header header--black">
    <?php elseif (rwmb_meta('header_options', [], $thePost->ID) === 'clear'): ?>
    <header class="header header--clear">
        <?php else: ?>
        <header class="header header--white">
            <?php endif ?>
            <div class="hamburger-content hamburger-content--js">
                <div class="hamburger-content__container">
                    <?php
                    wp_nav_menu();
                    ?>
                    <p class="address">
                        info@swensonhe.com
                        424.465.2525
                        <br>
                        <br>
                        600 Corporate Pointe
                        <br>
                        Suite 1200
                        <br>
                        Culver City, CA 90230
                    </p>
                </div>
            </div>
            <div class="container">
                <a class="logo" href="<?php echo home_url(); ?>">
                    <?php if (rwmb_meta('header_options', [], $thePost->ID) === 'black' || rwmb_meta('header_options', [], $thePost->ID) === 'clear') {
                        echo "<img class='logo' src='" . get_bloginfo('template_url') . "/assets/Logo_White.png' >";
                    } else {
                        echo "<img class='logo' src='" . get_bloginfo('template_url') . "/assets/Logo_Primary.png' >";
                    } ?>
                </a>
                <div class="page-title">
                    <a href="javascript:window.location.reload(true)"><?php
                        if (is_home() && !is_front_page()) {
                            echo '&nbsp;&nbsp;/&nbsp;' . get_the_title(get_option('page_for_posts', true));
                        } else if (get_post_type() === 'people') {
                            echo '&nbsp;&nbsp;/&nbsp;About';
                        } else if (get_post_type() === 'post') {
                            echo '&nbsp;&nbsp;/&nbsp;Blog';
                        } else if (get_post_type() === 'work') {
                            echo '&nbsp;&nbsp;/&nbsp;Work';
                        } else if (!is_front_page() && !is_single()) {
                            if (!(get_page_template_slug() === 'ppc.php' || get_page_template_slug() === 'infographic.php')) {
                                echo '&nbsp;&nbsp;/&nbsp;' . get_the_title();
                            }
                        }
                        ?></a>

                </div>
                <nav>
                    <?php if (rwmb_meta('header_options', [], $thePost->ID) !== 'white-w/o-btn' || (rwmb_meta('header_options', [], $thePost->ID) === 'clear')): ?>
                        <?php if (is_front_page() || (rwmb_meta('header_options', [], $thePost->ID) === 'clear')): ?>
                            <a class="nav--contact btn btn--small btn--white" href="/contact/">contact us</a>
                        <?php else: ?>
                            <a class="nav--contact btn btn--small btn--grey" href="/contact/">contact us</a>
                        <?php endif ?>
                    <?php endif; ?>
                    <!--                    hamburger-->
                    <span id="nav-icon4" class="hamburger--js hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </span>
                </nav><!-- #site-navigation -->
            </div>
        </header><!-- #masthead -->
