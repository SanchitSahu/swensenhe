const ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: ['./js/app.js', './sass/style.scss'],
    output: {
        filename: 'dist/bundle.js'
    },
    module: {

        rules: [
            { // regular css files
                test: /\.css$/,
                loader: ExtractTextPlugin.extract(['style-loader', 'css-loader'])
            },
            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
            },
            {test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/, loader: 'url-loader'}
        ]
    },
    plugins: [
        new ExtractTextPlugin({ // define where to save the file
            filename: 'style.css',
            allChunks: true,
        }),
    ],
};