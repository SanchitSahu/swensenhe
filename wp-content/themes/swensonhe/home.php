<?php
/**
 * The blog template
 *
 * @package swensonhe
 */

get_header(); ?>

    <main>
        <div class="container">
            <div class="row">
                <h1 class="col-sm-9 col-xs-12 work-header">We explore trends, technologies, global consumer culture, and
                    insights for a changing world.
                </h1>
            </div>
            <div class="row">
                <?php
                if (have_posts()):
                /* Start the Loop */
                while (have_posts()) :
                the_post(); ?>
                <div class="col-sm-12">
                    <a class="blog-item-block" href="<?php the_permalink(); ?>"
                       title="<?php the_title_attribute(); ?>">
                        <div class="blog-item-block__description">
                            <p><?php echo get_the_date(); ?></p>
                            <h1><?php echo get_the_title(); ?></h1>
                            <p class="hidden-xs"><?php echo rwmb_meta('blog_list_short_description'); ?></p>
                            <p class="hidden-xs blog-item-block__description--author">
                                <strong><?php echo get_the_author(); ?></strong></p>
                        </div>
                        <div class="blog-item-block__img-container">
                            <img class="visible-xs-block" srcset="<?php
                            $image = rwmb_meta('blog_feature_image_small');
                            echo reset($image)['srcset']; ?>">
                            <img class="hidden-xs" srcset="<?php
                            $image = rwmb_meta('blog_feature_image_large');
                            echo reset($image)['srcset']; ?>">
                        </div>
                    </a>
                </div>
                <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </main>

<?php
get_footer();
