<?php
//if admin has meta plugin disabled, prevent Undefined Function Error
if (!function_exists('rwmb_meta')) {
    function rwmb_meta($key, $args = '', $post_id = null)
    {
        return false;
    }
}

//option for footer contact form
function footer_contact_get_meta_box($meta_boxes)
{
    $meta_boxes[] = array(
        'id' => 'footer_contact',
        'title' => esc_html__('Footer options', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'side',
        'priority' => 'default',
        'autosave' => true,
        'fields' => array(
            array(
                'id' => 'form_select',
                'name' => esc_html__('Contact Form', 'metabox-online-generator'),
                'type' => 'select_advanced',
                'placeholder' => esc_html__('Select an Item', 'metabox-online-generator'),
                'options' => array(
                    'none' => 'None',
                    'contact-white' => 'contact form with white background',
                    'contact-palm' => 'contact form with palm tree background',
                    'contact-palm-w/o-title' => 'contact form with palm tree background without title',
                ),
                'std' => 'none',
            ),
            array(
                'id' => 'footer_shortcode',
                'type' => 'text',
                'name' => esc_html__('footer_shortcode', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'footer_contact_get_meta_box');

function header_option_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'header_contact',
        'title' => esc_html__('Header Option Metabox', 'metabox-online-generator'),
        'post_types' => array('page', 'post', 'work'),
        'context' => 'side',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'header_options',
                'name' => esc_html__('header', 'metabox-online-generator'),
                'type' => 'select_advanced',
                'placeholder' => esc_html__('Select an Item', 'metabox-online-generator'),
                'options' => array(
                    'black' => 'black',
                    'white' => 'white',
                    'clear' => 'clear',
                    'white-w/o-btn' => 'white-w/o-btn',
                ),
                'std' => 'white',
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'header_option_get_meta_box');

// posting brief introduction field
function briefIntro_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'brief_intro',
        'title' => esc_html__('brief introduction', 'metabox-online-generator'),
        'post_types' => array('work'),
        'context' => 'normal',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'brief_intro',
                'type' => 'textarea',
                'name' => esc_html__('Textarea', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'briefIntro_get_meta_box');

// work posting demo block
function demo_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'demo',
        'title' => esc_html__('demo', 'metabox-online-generator'),
        'post_types' => array('work'),
        'context' => 'normal',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'demo_image_background',
                'type' => 'image_advanced',
                'name' => esc_html__('demo image background', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'demo_image_phone',
                'type' => 'image_advanced',
                'name' => esc_html__('demo image phone', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'demo_image_android',
                'type' => 'image_advanced',
                'name' => esc_html__('demo image android', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'android_link',
                'type' => 'text',
                'name' => esc_html__('android_link', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'demo_image_iphone',
                'type' => 'image_advanced',
                'name' => esc_html__('demo image iphone', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'app_link',
                'type' => 'text',
                'name' => esc_html__('app_link', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'demo_get_meta_box');

function about_carousel_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'about-photo-carousel',
        'title' => esc_html__('About Photo carousel', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => false,
        'include' => [
            'template' => 'about.php'
        ],
        'fields' => array(
            array(
                'id' => $prefix . 'about-photo-carousel-images',
                'type' => 'image_advanced',
                'name' => esc_html__('Image Advanced', 'metabox-online-generator'),
                'max_file_uploads' => '5',
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'about_carousel_get_meta_box');


function person_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'untitled',
        'title' => esc_html__('Person Metabox', 'metabox-online-generator'),
        'post_types' => array('people'),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'person_head_shot',
                'type' => 'image_advanced',
                'name' => esc_html__('Person Head Shot', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => $prefix . 'person_name',
                'type' => 'text',
                'name' => esc_html__('Name', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'person_title',
                'type' => 'text',
                'name' => esc_html__('Title', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'person_quote',
                'type' => 'textarea',
                'name' => esc_html__('Quote', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'person_order',
                'type' => 'number',
                'name' => esc_html__('Person Display Order(order on everywhere except landing)', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'person_order_landing',
                'type' => 'number',
                'name' => esc_html__('Person Display Order Landing (order on people landing page + filter page)', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'person_get_meta_box');

function services_get_meta_box($meta_boxes)
{

    $meta_boxes[] = array(
        'id' => 'untitled',
        'title' => esc_html__('Services Metabox', 'metabox-online-generator'),
        'post_types' => array('post', 'page'),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => false,
        'include' => [
            'template' => 'services.php'
        ],
        'fields' => array(
            array(
                'id' => 'challenge_header_0',
                'type' => 'text',
                'name' => esc_html__('First Challenge header', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_image_0',
                'type' => 'image_advanced',
                'name' => esc_html__('First Challenge Image', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'challenge_page_href_0',
                'type' => 'text',
                'name' => esc_html__('First Page of of href case study', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_copy_0',
                'type' => 'textarea',
                'name' => esc_html__('First Challenge Copy', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_header_1',
                'type' => 'text',
                'name' => esc_html__('2nd Challenge header', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_image_1',
                'type' => 'image_advanced',
                'name' => esc_html__('2nd Challenge Image', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'challenge_page_href_1',
                'type' => 'text',
                'name' => esc_html__('2nd Page of of href case study', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_copy_1',
                'type' => 'textarea',
                'name' => esc_html__('2nd Challenge Copy', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_header_2',
                'type' => 'text',
                'name' => esc_html__('3rd Challenge header', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_image_2',
                'type' => 'image_advanced',
                'name' => esc_html__('3rd Challenge Image', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'challenge_page_href_2',
                'type' => 'text',
                'name' => esc_html__('3rd Page of of href case study', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_copy_2',
                'type' => 'textarea',
                'name' => esc_html__('3rd Challenge Copy', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_header_3',
                'type' => 'text',
                'name' => esc_html__('4th Challenge header', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_image_3',
                'type' => 'image_advanced',
                'name' => esc_html__('4th Challenge Image', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'challenge_page_href_3',
                'type' => 'text',
                'name' => esc_html__('4th Page of of href case study', 'metabox-online-generator'),
            ),
            array(
                'id' => 'challenge_copy_3',
                'type' => 'textarea',
                'name' => esc_html__('4th Challenge Copy', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'services_get_meta_box');

//work posting details block
function details_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'details',
        'title' => esc_html__('details', 'metabox-online-generator'),
        'post_types' => array('work'),
        'context' => 'normal',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'details_title_businesstype',
                'type' => 'textarea',
                'name' => esc_html__('details_title_businessType', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'details_text_businesstype',
                'type' => 'textarea',
                'name' => esc_html__('details_text_businessType', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'details_image_businesstype',
                'type' => 'image_advanced',
                'name' => esc_html__('detasils_image_businessType', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'details_title_solution',
                'type' => 'textarea',
                'name' => esc_html__('details_title_solution', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'details_text_solution',
                'type' => 'textarea',
                'name' => esc_html__('details_text_solution', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'details_image_solution',
                'type' => 'image_advanced',
                'name' => esc_html__('details_image_solution', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'details_get_meta_box');


//work posting testimonial block
function testimonial_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'testimonial',
        'title' => esc_html__('testimonial', 'metabox-online-generator'),
        'post_types' => array('work'),
        'context' => 'normal',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'testimonial_image_profile',
                'type' => 'image_advanced',
                'name' => esc_html__('testimonial image profile', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'testimonial_text_description',
                'type' => 'textarea',
                'name' => esc_html__('testimonial text description', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'testimonial_text_profile',
                'type' => 'textarea',
                'name' => esc_html__('testimonial text profile', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'testimonial_image_other',
                'type' => 'image_advanced',
                'name' => esc_html__('testimonial image other', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'testimonial_image_other_lg',
                'type' => 'image_advanced',
                'name' => esc_html__('testimonial image other lg', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'testimonial_get_meta_box');

//work posting results block
function results_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'results',
        'title' => esc_html__('results', 'metabox-online-generator'),
        'post_types' => array('work'),
        'context' => 'normal',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'results',
                'type' => 'text',
                'name' => esc_html__('results', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'results_description',
                'type' => 'textarea',
                'name' => esc_html__('results description', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'results_image',
                'type' => 'image_advanced',
                'name' => esc_html__('results image', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'results_get_meta_box');

/*function slide1($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'slide1',
        'title' => esc_html__('Slide 1', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'include' => [
            'template' => 'front-page.php'
        ],
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'slide_1_work_page_id',
                'type' => 'text',
                'name' => esc_html__('Work Page ID', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'slide_1_copy',
                'type' => 'textarea',
                'name' => esc_html__('Copy', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'slide_1_logo',
                'type' => 'image_advanced',
                'name' => esc_html__('Logo', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_1_hero',
                'type' => 'image_advanced',
                'name' => esc_html__('Hero image', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_1_video_poster',
                'type' => 'image_advanced',
                'name' => esc_html__('Poster Image to show before video is loaded', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_1_video_mp4',
                'type' => 'video',
                'name' => esc_html__('Video mp4', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_1_video_webm',
                'type' => 'video',
                'name' => esc_html__('Video webm', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'slide1');

function slide2($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'slide2',
        'title' => esc_html__('Slide 2', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'include' => [
            'template' => 'front-page.php'
        ],
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'slide_2_work_page_id',
                'type' => 'text',
                'name' => esc_html__('Work Page ID', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'slide_2_copy',
                'type' => 'textarea',
                'name' => esc_html__('Copy', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'slide_2_logo',
                'type' => 'image_advanced',
                'name' => esc_html__('Logo', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_2_hero',
                'type' => 'image_advanced',
                'name' => esc_html__('Hero image', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_2_video_poster',
                'type' => 'image_advanced',
                'name' => esc_html__('Poster Image to show before video is loaded', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_2_video_mp4',
                'type' => 'video',
                'name' => esc_html__('Video mp4', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_2_video_webm',
                'type' => 'video',
                'name' => esc_html__('Video webm', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'slide2');

function slide3($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'slide3',
        'title' => esc_html__('Slide 3', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'include' => [
            'template' => 'front-page.php'
        ],
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => $prefix . 'slide_3_work_page_id',
                'type' => 'text',
                'name' => esc_html__('Work Page ID', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'slide_3_copy',
                'type' => 'textarea',
                'name' => esc_html__('Copy', 'metabox-online-generator'),
            ),
            array(
                'id' => $prefix . 'slide_3_logo',
                'type' => 'image_advanced',
                'name' => esc_html__('Logo', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_3_hero',
                'type' => 'image_advanced',
                'name' => esc_html__('Hero image', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_3_video_poster',
                'type' => 'image_advanced',
                'name' => esc_html__('Poster Image to show before video is loaded', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_3_video_mp4',
                'type' => 'video',
                'name' => esc_html__('Video mp4', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => $prefix . 'slide_3_video_webm',
                'type' => 'video',
                'name' => esc_html__('Video webm', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'slide3');
*/
function home_logos_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => '',
        'title' => esc_html__('Awards and Recognition', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => false,
        'include' => [
            'template' => 'front-page.php'
        ],
        'fields' => array(
            array(
                'id' => 'home_client_logos',
                'type' => 'image_advanced',
                'max_file_uploads' => 5,
                'name' => esc_html__('Client Logos', 'metabox-online-generator'),
            ),
            array(
                'id' => 'home_awards',
                'type' => 'image_advanced',
                'max_file_uploads' => 8,
                'name' => esc_html__('App Rating pictures', 'metabox-online-generator'),
            ),
            array(
                'id' => 'info_graphic',
                'type' => 'image_advanced',
                'max_file_uploads' => 1,
                'name' => esc_html__('Info Graphic Image', 'metabox-online-generator'),
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'home_logos_get_meta_box');

function blog_list_get_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => '',
        'title' => esc_html__('Content', 'metabox-online-generator'),
        'post_types' => array('post'),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => 'blog_feature_image_small',
                'type' => 'image_advanced',
                'name' => esc_html__('Feature Image Small', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'blog_feature_image_large',
                'type' => 'image_advanced',
                'name' => esc_html__('Feature Image Large', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'blog_landing_image',
                'type' => 'image_advanced',
                'name' => esc_html__('Landing Image', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
            array(
                'id' => 'blog_list_short_description',
                'type' => 'textarea',
                'name' => esc_html__('Short Description', 'metabox-online-generator'),
            ),
            array(
                'id' => 'blog_last_image',
                'type' => 'image_advanced',
                'name' => esc_html__('Last Image after content', 'metabox-online-generator'),
                'max_file_uploads' => '1',
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'blog_list_get_meta_box');

function ppc_meta_box($meta_boxes)
{
    $prefix = '';

    $meta_boxes[] = array(
        'id' => 'ppc',
        'title' => esc_html__('Short Code for Form', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'priority' => 'default',
        'autosave' => false,
        'include' => [
            'template' => 'ppc.php'
        ],
        'fields' => array(
            array(
                'id' => $prefix . 'ppc_shortcode',
                'type' => 'text',
                'name' => 'Short Code ',
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'ppc_meta_box');



function ppc_images($meta_boxes)
{
    $meta_boxes[] = array(
        'id' => 'ppc_images',
        'title' => esc_html__('PPC images', 'metabox-online-generator'),
        'post_types' => array('page'),
        'context' => 'advanced',
        'include' => [
            'template' => 'ppc.php'
        ],
        'priority' => 'default',
        'autosave' => false,
        'fields' => array(
            array(
                'id' => 'ppc_images_small',
                'type' => 'image_advanced',
                'name' => esc_html__('Mobile Background Image', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => 'ppc_images_medium',
                'type' => 'image_advanced',
                'name' => esc_html__('Medium Background Image', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
            array(
                'id' => 'ppc_images_large',
                'type' => 'image_advanced',
                'name' => esc_html__('Large Background Image', 'metabox-online-generator'),
                'max_file_uploads' => 1,
            ),
        ),
    );

    return $meta_boxes;
}

add_filter('rwmb_meta_boxes', 'ppc_images');
