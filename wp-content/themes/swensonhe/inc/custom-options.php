<?php

// add the admin options page
add_action('admin_menu', 'plugin_admin_add_page');
function plugin_admin_add_page()
{
    add_options_page('Swenson He Global Options Page', 'Global Options Menu', 'manage_options', 'plugin', 'swenson_he_options_page');
}

function swenson_he_options_page()
{
    ?>
    <div>
        <form action="options.php" method="post">
            <?php settings_fields('swenson_he_options'); ?>
            <?php do_settings_sections('plugin'); ?>

            <input name="Submit" type="submit" value="<?php esc_attr_e('Save Changes'); ?>"/>
        </form>
    </div>

    <?php
}

add_action('admin_init', 'plugin_admin_init');
function plugin_admin_init()
{
    register_setting('swenson_he_options', 'swenson_he_options', 'swenson_he_options_validate');
    add_settings_section('plugin_main', 'Swenson He Global Options ', 'plugin_section_text', 'plugin');
    add_settings_field('plugin_text_string', 'Options', 'plugin_setting_string', 'plugin', 'plugin_main');
}

function plugin_section_text()
{
    echo '<p>Global Options shared across the site</p>';
}

function plugin_setting_string()
{
    $options = get_option('swenson_he_options');
    echo "<label>Business Form Shortcode</label><input name='swenson_he_options[contact_form]' size='100' type='text' value='{$options['contact_form']}' /><br>";
    echo "<label>Career Form Shortcode</label><input name='swenson_he_options[career_form]' size='100' type='text' value='{$options['career_form']}' /><br>";
    echo "<label>Subscribe Form Shortcode</label><input name='swenson_he_options[subscribe_form]' size='100' type='text' value='{$options['subscribe_form']}' /><br>";
    echo "<label>Facebook Link</label><input name='swenson_he_options[facebook_link]' size='100' type='text' value='{$options['facebook_link']}' /><br>";
    echo "<label>Instagram Link</label><input name='swenson_he_options[instagram_link]' size='100' type='text' value='{$options['instagram_link']}' /><br>";
    echo "<label>Linkedin Link</label><input name='swenson_he_options[linkedin_link]' size='100' type='text' value='{$options['linkedin_link']}' /><br>";
    echo "<label>Twitter Link</label><input name='swenson_he_options[twitter_link]' size='100' type='text' value='{$options['twitter_link']}' /><br>";
    echo "<label>Email</label><input name='swenson_he_options[email]' size='100' type='text' value='{$options['email']}' /><br>";
    echo "<label>Phone Number</label><input name='swenson_he_options[phone]' size='100' type='text' value='{$options['phone']}' /><br>";
}

function swenson_he_options_validate($input)
{
    $options = get_option('swenson_he_options');
    $options['contact_form'] = trim($input['contact_form']);
    $options['career_form'] = trim($input['career_form']);

    foreach ($input as $key => $value) {
        $options[$key] = trim($input[$key]);
    }
    return $options;
}
