<?php
//hook into the init action and call create_job_type_taxonomy when it fires
add_action('init', 'create_job_type_taxonomy', 0);

//create a custom taxonomy name it topics for your posts

function create_job_type_taxonomy()
{

// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

    $labels = array(
        'name' => _x('Job Types', 'taxonomy general name'),
        'singular_name' => _x('Job Type', 'taxonomy singular name'),
        'search_items' => __('Search Job Types'),
        'all_items' => __('All Job Types'),
        'parent_item' => __('Parent Job Type'),
        'parent_item_colon' => __('Parent Job Type:'),
        'edit_item' => __('Edit Job Type'),
        'update_item' => __('Update Job Type'),
        'add_new_item' => __('Add New Job Type'),
        'new_item_name' => __('New Job Type Name'),
        'menu_name' => __('Job Types'),
    );

// Now register the taxonomy

    register_taxonomy('job_type', array('post'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'job_type'),
    ));

}


/*
* Creating a function to create People post type
*/

function people_post_type()
{

// Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('People', 'Post Type General Name', 'swensonhe'),
        'singular_name' => _x('Person', 'Post Type Singular Name', 'swensonhe'),
        'menu_name' => __('People', 'swensonhe'),
        'parent_item_colon' => __('Parent People', 'swensonhe'),
        'all_items' => __('All People', 'swensonhe'),
        'view_item' => __('View People', 'swensonhe'),
        'add_new_item' => __('Add New People', 'swensonhe'),
        'add_new' => __('Add New', 'swensonhe'),
        'edit_item' => __('Edit People', 'swensonhe'),
        'update_item' => __('Update People', 'swensonhe'),
        'search_items' => __('Search People', 'swensonhe'),
        'not_found' => __('Not Found', 'swensonhe'),
        'not_found_in_trash' => __('Not found in Trash', 'swensonhe'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label' => __('people', 'swensonhe'),
        'description' => __('People news and reviews', 'swensonhe'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('genres'),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => false,
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'taxonomies' => array('job_type'),
    );

    // Registering your Custom Post Type
    register_post_type('people', $args);

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action('init', 'people_post_type', 0);


function create_work_type_taxonomy()
{
    $labels = array(
        'name' => _x('Work Types', 'taxonomy general name'),
        'singular_name' => _x('Work Type', 'taxonomy singular name'),
        'search_items' => __('Search Work Types'),
        'all_items' => __('All Work Types'),
        'parent_item' => __('Parent Work Type'),
        'parent_item_colon' => __('Parent Work Type:'),
        'edit_item' => __('Edit Work Type'),
        'update_item' => __('Update Work Type'),
        'add_new_item' => __('Add New Work Type'),
        'new_item_name' => __('New Work Type Name'),
        'menu_name' => __('Work Types'),
    );

// Now register the taxonomy

    register_taxonomy('work_type', 'work', array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
//        'show_tagcloud' => false,
        'rewrite' => array('slug' => 'work', 'with_front' => false)
    ));
}

add_action('init', 'create_work_type_taxonomy', 0);

function work_post_type()
{

// Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Work', 'Post Type General Name', 'swensonhe'),
        'singular_name' => _x('Work', 'Post Type Singular Name', 'swensonhe'),
        'menu_name' => __('Work', 'swensonhe'),
        'parent_item_colon' => __('Parent Work', 'swensonhe'),
        'all_items' => __('All Work', 'swensonhe'),
        'view_item' => __('View Work', 'swensonhe'),
        'add_new_item' => __('Add New Work', 'swensonhe'),
        'add_new' => __('Add New', 'swensonhe'),
        'edit_item' => __('Edit Work', 'swensonhe'),
        'update_item' => __('Update Work', 'swensonhe'),
        'search_items' => __('Search Work', 'swensonhe'),
        'not_found' => __('Not Found', 'swensonhe'),
        'not_found_in_trash' => __('Not found in Trash', 'swensonhe'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label' => __('work', 'swensonhe'),
        'description' => __('Work news and reviews', 'swensonhe'),
        'labels' => $labels,
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
        'taxonomies' => array('work_type', 'post_tag'),
        'rewrite' => array('slug' => 'work/%work_type%', 'with_front' => false),
        'has_archive' => 'work',
    );

    // Registering your Custom Post Type
    register_post_type('work', $args);

}

add_action('init', 'work_post_type', 0);

function wpa_show_permalinks($post_link, $post)
{
    if (is_object($post) && $post->post_type == 'work') {
        $terms = wp_get_object_terms($post->ID, 'work_type');
        if ($terms) {
            return str_replace('%work_type%', $terms[0]->slug, $post_link);
        }
    }
    return $post_link;
}

add_filter('post_type_link', 'wpa_show_permalinks', 1, 2);