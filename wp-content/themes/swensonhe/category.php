<?php
/**
 * The category template
 *
 * @package swensonhe
 */

get_header(); ?>

    <main>
        <div class="container">
            <div class="row">
                <h1 class="col-sm-9 col-md-12 col-xs-12 work-header">Here is some of our work we are really proud
                    of.</h1>
            </div>
            <div class="category-list visible-md-block-and-up">
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>">All</a>
                <?php
                $currentCategory = get_the_category()[0];
                foreach (get_categories() as $category):
                    echo '<a href="' . get_category_link($category->term_id) . '" class=' . ($currentCategory->term_id == $category->term_id ? 'active' : '') . '>' . $category->name . '</a>';
                endforeach; ?>
            </div>
            <?php
            $count = 0;
            /* Start the Loop */
            while (have_posts()) :
                the_post();
                if ($count % 2 == 0): ?>
                    <div class="row">
                <?php endif;
                ?>
                <div class="col-sm-6 works-landing">
                    <?php
                    get_template_part('template-parts/content-work-landing', get_post_format());
                    ?>
                </div>
                <?php
                if ($count % 2 == 1): ?>
                    </div>
                <?php endif;
                $count++;
            endwhile;
            if ($count % 2 == 1): ?>
        </div>
        <?php endif;
        ?>

        </div>
    </main>

<?php
get_footer();
