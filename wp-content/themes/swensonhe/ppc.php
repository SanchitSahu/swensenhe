<?php
/* Template Name: PPC */

get_header(); ?>
    <main>
        <div class="container-fluid ppc-picture-container">
            <div class="row ppc-picture-container__row">
                <picture>
                    <?php
                    $smallImage = rwmb_meta('ppc_images_small');
                    $smallImage = reset($smallImage);
                    $mediumImage = rwmb_meta('ppc_images_medium');
                    $mediumImage = reset($mediumImage);
                    $largeImage = rwmb_meta('ppc_images_large');
                    $largeImage = reset($largeImage);
                    ?>
                    <source media="(min-width: 1024px)" srcset="<?php echo $largeImage['full_url'];?>">
                    <source media="(min-width: 768px)" srcset="<?php echo $mediumImage['full_url']; ?>">
                    <img class="contact-picture" src="<?php echo $smallImage['full_url'];?>">
                </picture>
            </div>
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 contact-form-col">
                            <div class="ppc-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h1><?php echo get_the_title(); ?></h1>
                                        <?php echo $post->post_content; ?>
                                    </div>
                                    <div class="col-lg-6">
                                        <?php echo do_shortcode(rwmb_meta('ppc_shortcode')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
<?php
get_footer();
