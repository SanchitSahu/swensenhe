<?php
/**
 * The works template
 *
 * @package swensonhe
 */

get_header(); ?>

    <main>
        <div class="container">
            <div class="row">
                <h1 class="col-sm-9 col-md-12 col-xs-12 work-header">Here is some of our work we are really proud
                    of.</h1>
            </div>
            <div class="category-list hidden-xs">
                <a class="active" href="<?php echo get_post_type_archive_link('work'); ?>">All</a>
                <?php
                $terms = get_terms([
                    'post_type' => 'work_type',
                ]);

                $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                foreach ( $terms as $category):
                    echo '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
                endforeach; ?>
            </div>
            <?php
            if (have_posts()):
            $count = 0;
            /* Start the Loop */
            while (have_posts()) :
                the_post();
                if ($count % 2 == 0): ?>
                    <div class="row">
                <?php endif; ?>
                <div class="col-sm-6 works-landing">
                    <?php
                    get_template_part('template-parts/content-work-landing', get_post_format());
                    ?>
                </div>
                <?php
                if ($count % 2 == 1): ?>
                    </div>
                <?php endif;
                $count++;
            endwhile;
            if ($count % 2 == 1): ?>
        </div>
        <?php endif;
        endif; ?>

        </div>
    </main>

<?php
get_footer();
