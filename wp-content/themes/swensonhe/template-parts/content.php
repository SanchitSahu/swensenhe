<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swensonhe
 */

?>

<div class="container-fluid">
    <?php
    $demoBcImages = rwmb_meta('blog_landing_image', array('size' => 'full'));
    $demoBcImage = reset($demoBcImages);
    ?>
    <div class="row landing-img" style="background-image: url('<?php echo $demoBcImage['url'] ?>');"></div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4 visible-md-block-and-up blog-side-bar">
            <div class="blog-side-bar__container">
                <div class="blog-side-bar__avatar">
                    <?php echo get_avatar(get_the_author_meta('ID'), 100); ?>
                </div>
                <strong><?php echo get_the_author(); ?></strong>
                <p><?php echo get_the_date(); ?></p>
                <span class="grey-divider"></span>
            </div>
        </div>
        <div class="col-md-8 blog-description">
            <p class="blog-description__date"><?php echo get_the_date(); ?></p>
            <h1><?php the_title_attribute(); ?></h1>
            <?php the_content(); ?>
            <img srcset="<?php
            $image = rwmb_meta('blog_last_image');
            echo reset($image)['srcset']; ?>">

            <span class="grey-divider"></span>
            <div class="blog-social-block">
                <div class="blog-social-block__share">
                    <h5>Share</h5>
                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                            <path fill="#B1B1B1" fill-rule="evenodd"
                                  d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm18.205 9.429V17.18h2.553l.339-3.187h-2.892l.005-1.595c0-.832.079-1.277 1.273-1.277h1.596V7.933h-2.554c-3.067 0-4.146 1.546-4.146 4.147v1.913h-1.912v3.187h1.912v9.249h3.826z"/>
                        </svg>
                    </a>
                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo urlencode(get_permalink()); ?>&title=<?php echo get_the_title();?>">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                            <path fill="#B1B1B1" fill-rule="evenodd"
                                  d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm12.014-2.92H8.16v11.576h3.853V14.08zm.253-3.58c-.025-1.136-.836-2-2.154-2s-2.18.864-2.18 2c0 1.11.836 2 2.13 2h.025c1.343 0 2.18-.89 2.18-2zm13.64 8.519c0-3.556-1.9-5.21-4.435-5.21-2.046 0-2.961 1.123-3.473 1.911v-1.64h-3.853c.051 1.087 0 11.576 0 11.576H18V19.19c0-.346.025-.69.127-.939.279-.69.913-1.406 1.977-1.406 1.395 0 1.952 1.061 1.952 2.617v6.193h3.852v-6.637z"/>
                        </svg>
                    </a>
                    <a href="https://twitter.com/home?status=<?php echo urlencode(get_permalink()); ?>">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 34">
                            <path fill="#B1B1B1" fill-rule="evenodd"
                                  d="M0 17C0 7.611 7.611 0 17 0s17 7.611 17 17-7.611 17-17 17S0 26.389 0 17zm16.49-3.182l.037.588-.595-.072c-2.164-.276-4.055-1.212-5.66-2.785l-.785-.78-.202.576c-.428 1.284-.155 2.64.737 3.553.476.504.369.576-.452.276-.285-.096-.535-.168-.559-.132-.083.084.202 1.177.428 1.609.31.6.94 1.188 1.63 1.537l.582.276-.69.012c-.665 0-.69.012-.618.264.238.78 1.177 1.608 2.224 1.969l.737.252-.642.384a6.695 6.695 0 0 1-3.187.888c-.535.012-.975.06-.975.096 0 .12 1.45.793 2.295 1.057 2.533.78 5.541.444 7.8-.889 1.606-.948 3.211-2.833 3.96-4.658.405-.972.809-2.749.809-3.601 0-.552.036-.624.702-1.285.392-.384.76-.804.832-.924.119-.228.107-.228-.5-.024-1.01.36-1.153.312-.654-.228.37-.384.81-1.08.81-1.285 0-.036-.18.024-.381.133-.215.12-.69.3-1.047.408l-.642.204-.583-.396c-.32-.216-.773-.457-1.01-.529-.607-.168-1.534-.144-2.081.048-1.487.54-2.426 1.933-2.32 3.458z"/>
                        </svg>
                    </a>
                </div>
                <div class="blog-social-block__email">
                    <h5>Subscribe</h5>
                    <?php
                    echo do_shortcode((get_option('swenson_he_options')['subscribe_form']));
                    ?>
                </div>
            </div>
            <span class="grey-divider"></span>

        </div>
    </div>
    <!--    related articles-->
    <div class="related-articles">
        <h1 class="text-center">Other Articles</h1>
        <div class="row">
            <?php
            // related posts
            $related = custom_adjacent_posts_links(false, 2, 'post');
            if ($related) foreach ($related as $post) {
                setup_postdata($post); ?>
                <div class="col-sm-6">
                    <div class="related-articles__card">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <div class="related-articles__image-container">
                                <img srcset="<?php
                                $image = rwmb_meta('blog_feature_image_small');
                                echo reset($image)['srcset']; ?>">
                            </div>
                        </a>
                        <div class="related-articles__description">
                            <div class="visible-md-block-and-up">
                                <?php echo get_avatar(get_the_author_meta('ID'), 40); ?>
                                <span class="related-articles__author">
                                    <p>
                                    <?php echo get_the_author(); ?>
                                        <br>
                                        <?php echo get_the_date(); ?>
                                    </p>
                                </span>
                            </div>
                            <h5><?php the_title(); ?></h5>
                            <p class="visible-md-block-and-up related-articles__short-description"><?php echo rwmb_meta('blog_list_short_description'); ?></p>
                        </div>
                    </div>
                </div>
            <?php }
            wp_reset_postdata(); ?>
        </div>
    </div>
</div>
