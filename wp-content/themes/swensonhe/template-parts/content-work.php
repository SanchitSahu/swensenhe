<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swensonhe
 */

?>

<div class="works-content__description">
    <div class="container">
        <div class="brief-intro">
        <h1><?php echo rwmb_meta( 'brief_intro'); ?></h1>
    </div>
    </div>
    <?php
    $demoBcImages = rwmb_meta( 'demo_image_background', array( 'size' => 'full') );
    $demoBcImage = reset( $demoBcImages );
    ?>
    <div class="container-fluid">
        <div class="demo">
        <div class="demo__container" style="background-image: url('<?php echo $demoBcImage['url'] ?>');">
            <div class="demo__phone">
                <?php
                $demoPhoneImages = rwmb_meta( 'demo_image_phone', array( 'size' => 'full') );
                $demoPhoneImage = reset( $demoPhoneImages );
                ?>
                <img src="<?php echo $demoPhoneImage['url']; ?>">
            </div>
            <div class="demo__icons">
                <?php
                $demoAndroidImages = rwmb_meta( 'demo_image_android', array( 'size' => 'featured-medium') );
                $demoAndroidImage = reset( $demoAndroidImages );
                if( $demoAndroidImage):
                ?>
                <span>
                    <a href="<?php echo rwmb_meta( 'android_link'); ?>"><img src="<?php echo $demoAndroidImage['url']; ?>"></a>
                </span>
                <?php endif; ?>
                <?php
                $demoiPhoneImages = rwmb_meta( 'demo_image_iphone', array( 'size' => 'featured-medium') );
                $demoiPhoneImage = reset( $demoiPhoneImages );
                if($demoiPhoneImage):
                ?>
                <span>
                    <a href="<?php echo rwmb_meta( 'app_link'); ?>"><img src="<?php echo $demoiPhoneImage['url']; ?>"></a>
                </span>
                <?php endif; ?>
            </div>
        </div>
    </div>
    </div>
    <div class="container">
        <div class="details info">
        <div>
            <div class="info__title">
                <?php echo rwmb_meta( 'details_title_businesstype');?>
            </div>
            <div class="info__text">
                <?php echo rwmb_meta( 'details_text_businesstype');?>
            </div>
            <?php
                $businessImages = rwmb_meta( 'details_image_businesstype', array( 'size' => 'full') );
                $businessImage = reset( $businessImages );
                $solutionImages = rwmb_meta( 'details_image_solution', array( 'size' => 'full') );
                $solutionImage = reset( $solutionImages );
                if($businessImage && $solutionImage):
            ?>
                <div class="info__image">
                    <img src="<?php echo $businessImage['url']; ?>">
                </div>
                <?php elseif($businessImage): ?>
                <div class="info__image singleWidth">
                    <img src="<?php echo $businessImage['url']; ?>">
                </div>
            <?php endif; ?>
        </div>
        <div>
            <div class="info__title">
                <?php echo rwmb_meta( 'details_title_solution');?>
            </div>
            <div class="info__text">
                <?php echo rwmb_meta( 'details_text_solution');?>
            </div>
            <?php if($businessImage && $solutionImage): ?>
                <div class="info__image">
                    <img src="<?php echo $solutionImage['url']; ?>">
                </div>
            <?php elseif($solutionImage): ?>
                <div class="info__image singleWidth">
                    <img src="<?php echo $solutionImage['url']; ?>">
                </div>
            <?php endif; ?>
        </div>
    </div>
        <?php
            $profileImages = rwmb_meta( 'testimonial_image_profile', array( 'size' => 'profile-small') );
            $profileImage = reset( $profileImages );
            $testimoniaDescription = rwmb_meta( 'testimonial_text_description');
            if($testimoniaDescription) :?>
            <div class="testimonial">
                <div class="testimonial__text-wrapper">
                    <?php if($profileImage): ?>
                        <div class="testimonial__img-wrapper">
                            <img src="<?php echo $profileImage['url']; ?>" class="testimonial__img">
                        </div>
                    <?php endif; ?>
                    <div>
                        <?php if($profileImage): ?>
                            <div class="testimonial__text"><span><?php echo $testimoniaDescription;?></span></div>
                        <?php else: ?>
                            <div class="testimonial__text center"><span><?php echo $testimoniaDescription;?></span></div>
                        <?php endif; ?>
                        <div class="testimonial__profile"><?php echo rwmb_meta( 'testimonial_text_profile');?></div>
                    </div>
                </div>
                <?php
                $testimonialImages = rwmb_meta( 'testimonial_image_other', array( 'size' => 'full'));
                $testimonialImage = reset( $testimonialImages );
                $testimonialImagesLg = rwmb_meta( 'testimonial_image_other_lg', array( 'size' => 'full'));
                $testimonialImageLg = reset( $testimonialImagesLg );
                if($testimonialImage):
                ?>
                    <div class="testimonial__img-other">
                        <picture>
                            <source media="(max-width: 1919px)" srcset="<?php echo $testimonialImage['url']; ?> ">
                            <source media="(min-width: 1920px)" srcset="<?php echo $testimonialImageLg['url']; ?> ">
                            <img src="<?php echo $testimonialImage['url']; ?>">
                        </picture>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif;?>

        <?php
            $resultsDescripion = rwmb_meta( 'results_description');
            if($resultsDescripion):
        ?>
            <div class="results info">
                <div>
                    <div class="info__title"><?php echo rwmb_meta( 'results');?></div>
                    <?php echo '<div class="info__text">' . $resultsDescripion . '</div>'; ?>
                </div>
                <?php
                    $resultsImages = rwmb_meta( 'results_image', array( 'size' => 'full'));
                    $resultsImage = reset( $resultsImages );
                    if($resultsImage): ?>
                        <div class="info__image">
                            <img src="<?php echo $resultsImage['url']; ?>">
                        </div>
                    <?php endif; ?>
            </div>
        <?php endif; ?>
        <div class="related">
            <div class="related__title">Other Projects</div>
            <div class="row">
                <?php
                // related posts
                $related = custom_adjacent_posts_links( false, 2 );
                if( $related ) foreach( $related as $post ) {
                setup_postdata($post); ?>
                    <div class="col-sm-6 works-landing">
                        <?php
                        get_template_part('template-parts/content-work-landing', get_post_format());
                        ?>
                    </div>
                <?php }
                wp_reset_postdata();?>
            </div>
        </div>
    </div>
</div>
