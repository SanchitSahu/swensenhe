<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package swensonhe
 */

?>
<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
    <?php the_post_thumbnail(); ?>
</a>
<div class="works-landing__description">
    <?php the_title('<h5><a href="' . get_permalink() . '">', '</a></h5>'); ?>
    <div class="hidden-xxs">
        <?php
        $posttags = get_the_tags();
        if ($posttags) {
            echo "<p>";
            foreach ($posttags as $key => $tag) {
                echo $tag->name . ($key < sizeof($posttags) - 1 ? ', ' : '');
            }
            echo "</p>";
        }
        ?>
    </div>
</div>
<div class="view-project">
    <i class="line-horizontal" aria-hidden="true"></i> view project
</div>