<?php
/* Template Name: About */
get_header(); ?>

    <main>
        <!--Start Top Section Dynamic Part-->
        <div class="container about-title-block">
            <div class="container">
                <div class="about-people-header row">
                    <div class="col-md-8">
                        <h1>
                            <?php
                                if (get_field('top_section_blue_title')) {
                                    echo '<span class="blue">'.get_field('top_section_blue_title').'</span>';
                                } 
                                if (get_field('top_section_black_title')) {
                                    echo get_field('top_section_black_title');
                            } ?>
                        </h1>
                    </div>
                </div>
            </div>
        </div>
        <!--End Top Section Dynamic Part-->
        
        <!--Life style Slider-->
        <div class="container--margin">

            <div id="carousel-about-lifestyle" class="carousel carousel-about-lifestyle slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators carousel-indicators--inverted">
                    <li data-target="#carousel-about-lifestyle" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-about-lifestyle" data-slide-to="1"></li>
                    <li data-target="#carousel-about-lifestyle" data-slide-to="2"></li>
                    <li data-target="#carousel-about-lifestyle" data-slide-to="3"></li>
                    <li data-target="#carousel-about-lifestyle" data-slide-to="4"></li>
                </ol>
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                        $index = 0;
                        foreach (rwmb_meta('about-photo-carousel-images') as $image) {
                            if ($index === 0) {
                                echo "<div class='item active'>";
                            } else {
                                echo "<div class='item'>";
                            }
                            echo "<img srcset='" . $image['srcset'] . "'>";
                            echo "</div>";
                            $index++;
                        } 
                    ?>
                </div>
            </div>

        </div>
        
        <!--Our Team Slider-->
        
        <div class="container">
            <h1 class="text-center homepage-section-header">
                Leadership
            </h1>
            <?php
                $args = array('post_type' => 'people', 
                              'posts_per_page' => 5, 
                              'orderby' => 'meta_value_num', 
                              'meta_key' => 'person_order', 
                              'order' => 'ASC',);
                $the_query = new WP_Query($args); 
                if ($the_query->have_posts()): 
            ?>
                <div class="row person-list">
                    <?php
                        while ($the_query->have_posts()):
                        $the_query->the_post(); 
                    ?>
                        <div class="col-sm-4 person-block">
                            <div class="person-block__content">
                                <div class="person-block__image-container">
                                    <div class="person-block__quote text-center">
                                        <h1>
                                            <?php
                                                echo rwmb_meta('person_quote'); 
                                            ?>
                                        </h1>
                                    </div>
                                <div>
                                    <img srcset="<?php $image = rwmb_meta('person_head_shot');
                                        echo reset($image) ['srcset']; ?>">
                                    </div>
                                </div>
                                <div class="person-block__description">
                                    <h5><?php echo rwmb_meta('person_name'); ?></h5>
                                    <p><?php echo rwmb_meta('person_title'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                        endwhile; ?>
                    <?php
                        wp_reset_postdata(); ?>
                    <div class="col-sm-4 person-block">
                        <div class="person-block__content btn">
                            <div class="person-block__see">
                                    <a href="<?php echo get_post_type_archive_link('people'); ?>">See Our People</a>
                            </div>
                            <div class="person-block__see-filler"></div>
                        </div>
                    </div>
                    </div>
                </div>
            <?php
                endif; 
            ?>
        </div>
    </main>

<?php
get_footer();
