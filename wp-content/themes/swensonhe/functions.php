<?php
/**
 * swenson_he functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package swensonhe
 */
if (!function_exists('swenson_he_setup')):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function swenson_he_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on _s, use a find and replace
         * to change '_s' to the name of your theme in all the template files.
        */
        load_theme_textdomain('_s', get_template_directory() . '/languages');
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');
        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
        */
        add_theme_support('title-tag');
        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
        */
        add_theme_support('post-thumbnails');
        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array('menu-1' => esc_html__('Primary', '_s'),));
        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
        */
        add_theme_support('html5', array('search-form', 'comment-form', 'comment-list', 'gallery', 'caption',));
        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('_s_custom_background_args', array('default-color' => 'ffffff', 'default-image' => '',)));
        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');
    }
endif;
add_action('after_setup_theme', 'swenson_he_setup');
/**
 * Enqueue scripts and styles.
 */
function scripts() {
    wp_enqueue_style('style.css', get_stylesheet_uri());
    wp_enqueue_script('scrollify', get_template_directory_uri() . '/vendor/Scrollify-master/jquery.scrollify.js', null, null, true);
    wp_enqueue_script('scrollreveal', 'https://unpkg.com/scrollreveal/dist/scrollreveal.min.js', null, null, true);
    wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/vendor/bootstrap/js/bootstrap.min.js', null, null, true);
    wp_enqueue_script('bundle', get_template_directory_uri() . '/dist/bundle.js', null, null, true);
    wp_enqueue_script('swiperjs', get_template_directory_uri() . '/js/swiper.min.js', null, null, true);
    wp_enqueue_script('aosjs', get_template_directory_uri() . '/js/aos.js', null, null, true);
    wp_enqueue_script('customjs', get_template_directory_uri() . '/js/custom.js', null, null, true);
}
add_action('wp_enqueue_scripts', 'scripts');
/**
 * Add meta boxes
 */
require get_template_directory() . '/inc/meta-boxes.php';
/**
 * Add options
 */
require get_template_directory() . '/inc/custom-options.php';
/**
 * Add posts
 */
require get_template_directory() . '/inc/custom-posts.php';
function my_filter_head() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'my_filter_head');
add_image_size('largest image in container', 1600, 3000);
add_filter('post_thumbnail_html', 'remove_thumbnail_width_height', 10, 5);
function remove_thumbnail_width_height($html, $post_id, $post_thumbnail_id, $size, $attr) {
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}
// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');
// image resizing to serve meta box
add_theme_support('post-thumbnails');
add_image_size('featured-medium', 606, 360, true);
add_image_size('profile-small', 150, 150, true);
// other projects next 2
// Note: query is based on following plugin:
// Posts Order and Post Types Objects Order using a Drag and Drop Sortable javascript capability Version 1.9.3.6 | By Nsp Code
function custom_get_adjacent_posts($previous = true, $limit = 2, $type) {
    global $post, $wpdb;
    $op = $previous ? '<' : '>';
    if ($previous && $limit < 2) {
        $posts = $wpdb->get_results($wpdb->prepare("SELECT p.* FROM wp_posts AS p $join WHERE p.menu_order  $op $post->menu_order AND p.post_type = '$type' AND p.post_status = 'publish' ORDER BY p.menu_order DESC LIMIT $limit", $post->menu_order, $post->post_type));
    } else {
        $posts = $wpdb->get_results($wpdb->prepare("SELECT p.* FROM wp_posts AS p $join WHERE p.menu_order  $op $post->menu_order AND p.post_type = '$type' AND p.post_status = 'publish' ORDER BY p.menu_order ASC LIMIT $limit", $post->menu_order, $post->post_type));
    }
    return $posts;
}
function custom_adjacent_posts_links($previous = true, $limit = 2, $type = "work") {
    $prev_posts = custom_get_adjacent_posts($previous, $limit, $type);
    if (empty($prev_posts)) {
        return custom_get_adjacent_posts(true, $limit, $type);
    } elseif (count($prev_posts) < 2) {
        $prev = $prev_posts;
        $next = custom_get_adjacent_posts(true, 1, $type);
        return array_merge($next, $prev);
    } else {
        return $prev_posts;
    }
}
/*Start Services Custom post */

function services_post() {
    register_post_type('services', array('labels' => array('name' => __('Services'),), 'public' => true, 'hierarchical' => true, 'has_archive' => true, 'supports' => array('title', 'editor', 'excerpt',),));
    register_taxonomy_for_object_type('post_tag', 'services');
}
add_action('init', 'services_post');

/*End Services Custom Post*/

/*Start Custom field Display */

add_filter('acf/settings/remove_wp_meta_box', '__return_false');

/*End Custom field Display */

/*Start Custom Image Size */

add_image_size('post-thumb', 50, 50);

/*End Custom Image Size */
