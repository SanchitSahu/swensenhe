<?php
/* Template Name: infographic */

get_header(); ?>
    <main>
        <div class="container container--infographic">
            <div class="row">
                    <img class="infographic" src="<?php echo get_the_post_thumbnail_url(); ?>">
            </div>
        </div>
    </main>
<?php
get_footer();
