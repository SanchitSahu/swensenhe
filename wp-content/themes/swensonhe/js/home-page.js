(function ($) {
    $.initHomePage = function () {
        var timeout = false,
            delay = 250;

        $.scrollify({
            section: ".slide--js",
            scrollSpeed: 800,
            overflowScroll: true,
            setHeights: false,
            updateHash: false,
            before: function (index, slides) {
                //remove active class from all slides
                for (var i = 0; i < slides.length; i++) {
                    var slide = slides[i];
                    slide.removeClass('active')
                }

                //add active class to slide
                slide = slides[index];
                slide.addClass('active');
                 if(index > 0 && index < 5){
                    $(".desktop-slider__indicator").show();
                }else{
                    $(".desktop-slider__indicator").hide();
                }
                slide.find('.desktop-slider__description').children().each(function (i) {
                    var delay = ['ms300', 'ms350', 'ms400'];
                    $(this).addClass('fade-in-and-right ' + delay[i]);
                });

                //    toggle slide indicator
                $(".desktop-slide-indicator--js").each(function () {
                    $(this).removeClass('active');
                });

                var indicator = $(".desktop-slide-indicator--js[data-slide=" + index + "]");
                indicator.addClass('active');
            },
            afterRender: function () {
                var slide = $.scrollify.current();

                slide.addClass('active');
                slide.find('.desktop-slider__description').children().each(function (i) {
                    var delay = ['ms300', 'ms350', 'ms400'];
                    $(this).addClass('fade-in-and-right ' + delay[i]);
                });
                var indicator = $(".desktop-slide-indicator--js[data-slide=" + $.scrollify.currentIndex() + "]");
                indicator.addClass('active');

                getDimensions();
            },
            afterResize: function () {
                // clear the timeout
                clearTimeout(timeout);
                // start timing for event "completion"
                timeout = setTimeout(getDimensions, delay);
            }
        });

        $('.scroll-to-slider--js').on('click', function () {
            $.scrollify.next();
        });

        $('.desktop-slide-indicator--js').on('click', function (e) {
            $.scrollify.move($(e.target).data('slide'));
        })

        function getDimensions() {
            if ($(window).width() > 767 && $.scrollify.isDisabled() === true) {
                $.scrollify.enable();
                $.scrollify.move(0);
            } else if ($(window).width() <= 767 && $.scrollify.isDisabled() === false) {
                $.scrollify.disable();
            }
        }

        window.sr = ScrollReveal({reset: true});

        $(window).bind("pageshow", function(event) {
            if (event.originalEvent.persisted) {
                window.location.reload();
            }
        });

        $('video').on('ended', function () {
            this.load();
            this.play();
        });

    }
})(jQuery);
