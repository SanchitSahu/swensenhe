(function ($) {
    $.initServices = function () {
        //mobile animation
        var findMiddleElement = (function (docElm) {
            var viewportHeight = docElm.clientHeight,
                gradient = $(".gradient--js "),
                elements = $(".mobile-slide--js");

            return function (e) {
                var middleElement;
                if (e && e.type == 'resize')
                    viewportHeight = docElm.clientHeight;

                elements.each(function () {
                    var pos = (this.getBoundingClientRect().bottom + this.getBoundingClientRect().top) / 2;
                    // if an element is more or less in the middle of the viewport
                    if (pos > viewportHeight / 2.1 && pos < viewportHeight / 1.9) {
                        middleElement = this;
                        return false; // stop iteration
                    }
                });

                if (middleElement !== undefined) {
                    var slide = $(middleElement).data('slide');
                    switch (slide) {
                        case 0:
                            if (!gradient.hasClass('first')) {
                                gradient.removeClass('last');
                                gradient.removeClass('middle');
                                gradient.addClass('first');
                            }
                            break;
                        case 1:
                            if (!gradient.hasClass('middle')) {
                                gradient.removeClass('last');
                                gradient.removeClass('first');
                                gradient.addClass('middle');
                            }
                            break;
                        case 2:
                            if (!gradient.hasClass('last')) {
                                gradient.removeClass('first');
                                gradient.removeClass('middle');
                                gradient.addClass('last');
                            }
                            break;
                        default:
                            return;
                    }
                }
            }
        })(document.documentElement);

        $(window).on('scroll resize', findMiddleElement);

        //desktop animation

        var slides = $(".desktop-slide--js");
        var slideHeaders = $(".desktop-slide-header--js");
        var slideIndex = 1;
        var userHover = false;

        var checkAnimateSlides = function () {
            if (userHover) return;

            animateSlides();

            if (slideIndex === 2) {
                slideIndex = 0;
            }
            else {
                slideIndex++;
            }
        };

        var animateSlides = function () {
            slides.each(function (i, e) {
                $(e).removeClass('active');
                if (i === slideIndex) {
                    $(e).addClass('active');
                }
            });

            slideHeaders.each(function (i, e) {
                $(e).removeClass('active');
                if (i === slideIndex) {
                    $(e).addClass('active');
                }
            });
        };

        setInterval(checkAnimateSlides, 12 * 1000);

        // var hoverTargets = $(".hover-target--js")
        $(".hover-target--js").mouseover(function (event) {
            slideIndex = $(event.target).parent().data('slide');
            userHover = true;
            animateSlides();
        }).mouseout(function () {
            userHover = false;
        });

    }
})(jQuery);