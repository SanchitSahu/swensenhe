//put things here that are shared across the site
(function ($) {
    $().ready(function () {
        var hamburgerOpen = false;
        var navIcon = $('#nav-icon4');
        var hamburgerContent = $('.hamburger-content--js');
        var toggleNav = function() {
            hamburgerContent.toggleClass('open');
            navIcon.toggleClass('open');
            hamburgerOpen = !hamburgerOpen;
        }

        navIcon.click(toggleNav);
        hamburgerContent.click(toggleNav);

        $('.hamburger-content__container').click(function (e) {
            e.stopPropagation();
        });

        var iScrollPos = 0;

        $(window).scroll(function () {
            var iCurScrollPos = $(this).scrollTop();
            if (hamburgerOpen === false) {
                if (iCurScrollPos > iScrollPos) {
                    $('header').addClass('closed');
                } else {
                    $('header').removeClass('closed');
                }
            }
            iScrollPos = iCurScrollPos > 0 ? iCurScrollPos : 0;
        });
        window.sr = ScrollReveal({reset: true});
        sr.reveal('.fade-in-bottom--js', {
            duration: 800,
            distance: '0',
            easing: 'ease-in',
            scale: 1,
            viewFactor: 0.5
        });
    });
})(jQuery);