<?php
/**
 * Template Name: Front Page
 *
 * The front page template
 *
 * @package swensonhe
 */
get_header(); ?>
    <main>
        <div class="background-black">
            
            <!--Start Top Section Dynamic-->
            
            <div class="container">
                <div class="home-title row">
                    <div class="col-md-8 col-lg-7 fade-in-and-right ms300">
                        <h1><span class="light-blue never-outsourced">
                                <?php echo get_field('top_section_title'); ?>
                            </span><br>
                            <?php echo get_field('top_section_content'); ?>
                        </h1>
                        <div class="home-title__arrow bounce">
                            <button class="scroll-to-slider--js">
                                <i class="arrow-down" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            
            <!--End Top Section Dynamic-->

            <!--Start Slider Dynamic-->

            <div class="container-md container">
                
                <!--Start Desktop to Ipad Slider Dyanmic with design-->
                
                <div class="hidden-xs desktop-slider">
                    <div class="desktop-slider__phone-mock row slide-phone--js fade-in ms600">
                        <div class="col-xs-12 col-md-9 col-md-offset-3">
                            <div class="desktop-slider__indicator">
                                <?php 
                                    if (have_rows('slider')):
                                        $i = 1;
                                        while (have_rows('slider')):
                                            the_row();
                                            if (get_sub_field('active') == 'active'):
                                ?>
                                <i class="circle-grey desktop-slide-indicator--js" data-slide="<?php echo $i; ?>"></i>
                                <?php
                                         $i++;
                                            endif;
                                        endwhile;
                                    endif; 
                                ?>

                            </div>
                        </div>
                    </div>

                    

                        <!-- start slides with content-->
                    
                        <?php 
                            if (have_rows('slider')):
                                while (have_rows('slider')):
                                    the_row();
                                    if (get_sub_field('desktop_application_image') && get_sub_field('active') == 'active'):
                        ?>
                                    <!--Start Desktop Application Slider -->
                                        
                                        <div class="row desktop-slider__slide slide--js" data-section-name="<?php echo str_replace(" ", "-", get_sub_field('project_title')); ?>">
                                            <div class="col-xs-12 col-md-9 desktop-slider__image-container">
                                                <img srcset="<?php if (get_sub_field('hero_image')){ echo get_sub_field('hero_image');} ?>" class="desktop-slider__hero-img">

                                                  <div class="desktop-slider__device-container">
                                                    <img class="web-app-image" src="<?php if (get_sub_field('desktop_application_image')){ echo get_sub_field('desktop_application_image');} ?>">
                                                </div>

                                            </div>
                                            <div class="col-xs-3 desktop-slider__description">
                                                <a href="<?php if (get_sub_field('work_page_link')){ echo get_sub_field('work_page_link'); } ?>" class="desktop-slider__logo-img">
                                                    <img srcset="<?php if (get_sub_field('logo')){ echo get_sub_field('logo');}?>">
                                                </a>
                                                <p class="fade-in-and-right ms350"><?php if (get_sub_field('copy')){ echo get_sub_field('copy');} ?></p>
                                                <a class="btn btn--white" href="<?php if (get_sub_field('work_page_link')){ echo get_sub_field('work_page_link');} ?>">view project</a>
                                            </div>
                                        </div>
                                        
                                    <!--End Desktop Application Slider -->
                        <?php
                                    endif;
                                    if ((get_sub_field('video_mp4') || get_sub_field('video_webm')) && get_sub_field('active') == 'active'):
                        ?>
                                    <!--Start Mobile Application Video Slider-->
                                        
                                        <div class="row desktop-slider__slide slide--js" data-section-name="<?php echo str_replace(" ", "-", get_sub_field('project_title')); ?>">
                                            <div class="col-xs-12 col-md-9 desktop-slider__image-container">
                                                <div class="desktop-slider__phone-mock row slide-phone--js fade-in ms600">
                                                    <div class="col-xs-12">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/iphone.png">
                                                    </div>
                                                </div>
                                                <img srcset="<?php if (get_sub_field('hero_image')){ echo get_sub_field('hero_image'); } ?>" class="desktop-slider__hero-img">
                                                <video autoplay="" loop="" muted="" class="desktop-slider__phone-asset" poster="">
                                                    <source src="<?php echo get_sub_field('video_webm'); ?>" type="video/webm">
                                                    <source src="<?php echo get_sub_field('video_mp4'); ?>" type="video/mp4">
                                                </video>
                                            </div>
                                            <div class="col-xs-3 desktop-slider__description">
                                                <a href="<?php if (get_sub_field('work_page_link')){ echo get_sub_field('work_page_link'); } ?>" class="desktop-slider__logo-img">
                                                    <img srcset="<?php if (get_sub_field('logo')){ echo get_sub_field('logo');} ?>">
                                                </a>
                                                <p class="fade-in-and-right ms350"><?php if (get_sub_field('copy')){ echo get_sub_field('copy'); } ?></p>
                                                <a class="btn btn--white" href="<?php if (get_sub_field('work_page_link')){ echo get_sub_field('work_page_link');} ?>">view project</a>
                                            </div>
                                        </div>
                                        
                                    <!--End Mobile Application Video Slider-->
                        <?php
                                    endif;                        
                                    if(get_sub_field('mobile_application_image') && get_sub_field('active') == 'active'): ?>
                                    
                                    <!--Start Mobile Application Image Slider--> 
                                    
                                        <div class="row desktop-slider__slide slide--js" data-section-name="<?php echo str_replace(" ", "-", get_sub_field('project_title')); ?>">
                                            <div class="col-xs-12 col-md-9 desktop-slider__image-container">
                                                <div class="desktop-slider__phone-mock row slide-phone--js fade-in ms600">
                                                    <div class="col-xs-12">
                                                        <img src="<?php bloginfo('template_url'); ?>/assets/iphone.png">
                                                    </div>
                                                </div>
                                                <img srcset="<?php if (get_sub_field('hero_image')){ echo get_sub_field('hero_image'); } ?>" class="desktop-slider__hero-img">
                                                <img class="desktop-slider__phone-asset" src="<?php echo get_sub_field('mobile_application_image'); ?>">
                                            </div>
                                            <div class="col-xs-3 desktop-slider__description">
                                                <a href="<?php if (get_sub_field('work_page_link')){ echo get_sub_field('work_page_link');} ?>" class="desktop-slider__logo-img">
                                                    <img srcset="<?php if (get_sub_field('logo')){ echo get_sub_field('logo'); } ?>">
                                                </a>
                                                <p><?php if (get_sub_field('copy')){ echo get_sub_field('copy'); } ?></p>
                                                <a class="btn btn--white" href="<?php if (get_sub_field('work_page_link')){ echo get_sub_field('work_page_link'); } ?>">view project</a>
                                            </div>
                                        </div>
                                    
                                    <!--End Mobile Application Image Slider-->
                        <?php
                                    endif;
                                endwhile;
                            endif;
                        ?>
                    
                </div>
            </div>
            <!--End Desktop to Ipad Slider Dyanmic with design-->
            
            <!--Start Mobile Slider Dyanmic with design-->
            
            <div class="visible-xs-block">
                <div id="carousel-mobile-phone" class="carousel carousel-fade slide carousel-mobile-phone-block"
                     data-ride="carousel" data-interval="15000">
                    <!-- Start Indicators -->
                    <ol class="carousel-indicators carousel-indicators--dark">
                        <?php 
                            if (have_rows('slider')):
                                $mob_slider_indicator = 0;
                                while (have_rows('slider')):
                                    the_row();
                                    if (get_sub_field('active') == 'active'):
                        ?>
                                    <li data-target="#carousel-mobile-phone" data-slide-to="<?php echo $mob_slider_indicator; ?>" class="<?php if ($mob_slider_indicator == 0){ ?>active<?php } ?>"></li>
                        <?php
                                $mob_slider_indicator++;
                                    endif;
                                endwhile;
                            endif;
                        ?>
                    </ol>
                    <!-- End Indicators -->
                    
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php 
                            if (have_rows('slider')):
                                $mob_slider = 0;
                                while (have_rows('slider')):
                                    the_row();
                                    if (get_sub_field('desktop_application_image') && get_sub_field('active') == 'active'):
                        ?>
                                        <!--Start Desktop Application Slider-->
                                        
                                        <div class="item row <?php if ($mob_slider == 0) { echo 'active'; } ?>">
                                            <img class="carousel-mobile-phone-block__background-image" srcset="<?php echo get_sub_field('hero_image'); ?>">
                                            <img class="web-app-image" src="<?php echo get_sub_field('desktop_application_image'); ?>">
                           
                                            <a href="<?php echo $href; ?>">
                                                <img class="carousel-mobile-phone-block__logo-image" srcset="<?php echo get_sub_field('logo'); ?>">
                                            </a>
                                        </div>
                                        
                                        <!--End Desktop Application Slider-->
                        <?php
                                    endif;
                                    if ((get_sub_field('video_mp4') || get_sub_field('video_webm')) && get_sub_field('active') == 'active'):?>
                                        
                                        <!--Start Mobile Application video Slider-->
                                        
                                        <div class="item row <?php if ($mob_slider == 0){ echo 'active';} ?>">
                                            
                                            <img class="carousel-mobile-phone-block__background-image" srcset="<?php echo get_sub_field('hero_image'); ?>">
                                            <img class="carousel-mobile-phone-block__phone-mock-image" src="<?php bloginfo('template_url'); ?>/assets/iphone.png">
                                            <video autoplay="" loop="" muted="" class="carousel-mobile-phone-block__phone-image" poster="">
                                                <source src="<?php echo get_sub_field('video_webm'); ?>" type="<?php echo 'video/webm'; ?>">
                                                <source src="<?php echo get_sub_field('video_mp4'); ?>" type="<?php echo 'video/mp4'; ?>">
                                            </video>
                            
                                            <a href="<?php echo $href; ?>">
                                                <img class="carousel-mobile-phone-block__logo-image" srcset="<?php echo get_sub_field('logo'); ?>">
                                            </a>
                                        </div>
                                        
                                        <!--End Mobile Application video Slider-->
                        <?php
                                    endif;
                                    if (get_sub_field('mobile_application_image') && get_sub_field('active') == 'active'): ?>
                                        
                                        <!--Start Mobile Application Image Slider-->
                                        
                                        <div class="item row <?php if ($mob_slider == 0){ echo 'active';} ?>">
                                            <img class="carousel-mobile-phone-block__background-image" srcset="<?php echo get_sub_field('hero_image'); ?>">
                                            <img class="carousel-mobile-phone-block__phone-mock-image" src="<?php bloginfo('template_url'); ?>/assets/iphone.png">
                                            <img class="carousel-mobile-phone-block__phone-image" src="<?php echo get_sub_field('mobile_application_image'); ?>">
                            
                                            <a href="<?php echo $href; ?>">
                                                <img class="carousel-mobile-phone-block__logo-image" srcset="<?php echo get_sub_field('logo'); ?>">
                                            </a>
                                        </div>
                                        
                                        <!--End Mobile Application video Slider-->
                        <?php
                                    endif;
                                $mob_slider++;
                                endwhile;
                            endif;
                        ?>
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#carousel-mobile-phone" role="button" data-slide="prev">
                        <span class="arrow-left carousel-control__right" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-mobile-phone" role="button" data-slide="next">
                        <span class="arrow-right carousel-control__right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            
            <!--End Mobile Slider Dyanmic with design-->
            
        </div>
        
        <!--End Slider Dynamic-->
        
        
        
        <div class="slide--js background-white" data-section-name="rest">
            
            <!--Start Services Design and dynamic-->
            
            <div class="container-fluid"
                 style='background-image: url("<?php bloginfo('template_url'); ?>/assets/palm-tree.jpg")'>
                <div class="container--margin row">
                    <div class="card-block">
                        <div class="row">
                            <div class="col-lg-12">
                                <h1>We power the products we build with:</h1>
                            </div>
                        </div>
                        <div class="card-block__card-container">
                            <?php
                                $services_args = array('post_type' => 'services', 'order_by' => 'id', 'order' => 'ASC');
                                $services_list_loop = new WP_Query($services_args);
                                    if ($services_list_loop->have_posts()):
                                        while ($services_list_loop->have_posts()):
                                            $services_list_loop->the_post();
                            ?>
                            <div class="card card--home">
                                <div>
                                    <h5><?php the_title(); ?></h5>
                                    <p><?php the_excerpt(); ?></p>
                                    <?php 
                                    if (get_field('view_case_study_url')){?>
                                        <a href="<?php echo get_field('view_case_study_url'); ?>">View case study <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-arrow.svg" /></a>
                                    <?php
                                    }
                                    if (get_field('download_whitepaper')){?>
                                        <a href="<?php echo get_field('download_whitepaper'); ?>">Download White Paper <img src="<?php echo get_template_directory_uri(); ?>/assets/icons/icon-arrow.svg" /></a>
                                    <?php
                                    } 
                                    ?>
                                    <div class="icon-container">
                                        <i class="phone-icon icon-home" style="background: url('<?php echo get_field('icon') ?>') center no-repeat;"></i>
                                    </div>
                                </div>
                            </div>
                        <?php
                                         endwhile;
                                    endif;
                                    wp_reset_postdata();
                        ?>
                        </div>
                        <div class="card-block__link">
                            <a class="btn btn--grey btn--large" href="<?php echo esc_url(home_url('/services')); ?>">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <!--End Services Design and dynamic-->
            
            <div class="container-fluid background-black hidden-xs">
                <div class="container awards-block">
                    <h1 class="homepage-section-header">Awards & Recognition</h1>
                    <div class="row">
                        <?php
                            $index = 0;
                            foreach (rwmb_meta('home_client_logos') as $logo):
                                if ($index === 0): 
                                    echo '<div class="col-xs-4 col-lg-offset-1 col-lg-3" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">';
                                elseif ($index == 2): 
                                    echo '<div class="col-xs-4 col-lg-3" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">';
                                elseif ($index == 1): 
                                    echo '<div class="col-xs-4" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">';
                                else: 
                                    echo '<div class="col-xs-6" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">';
                                endif;
                        ?>
                                        <img class="awards-block__logos awards-block__logos--client" srcset="<?php echo $logo['srcset']; ?>">
                            
                        <?php
                            echo '</div>';
                            $index++;
                            endforeach; 
                        ?>
                    </div>
                    
                    <div class="row">
                    <?php
                        $index = 0;
                        foreach (rwmb_meta('home_awards') as $key => $award):
                            if ($index === 6): 
                                    echo '<div class="col-xs-4 col-xs-offset-2 col-lg-3 col-lg-offset-0 second-to-last-logo" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">';
                            else:
                                    echo '<div class="col-xs-4 col-lg-3" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">';
                            endif;
                    ?>
                                            <img class="awards-block__logos" srcset="<?php echo $award['srcset']; ?>">
                        
                    <?php
                    echo '</div>';
                        $index++;
                        endforeach; 
                    ?>
                    </div>

                   <!--Start Testimonails Dynamic--> 
            
                   <div class="row" data-aos="fade-zoom-in" data-aos-easing="ease-in-back" data-aos-offset="0">
                        <div id="carousel-quotes" class="carousel slide carousel-quotes carousel-fade col-lg-10 col-lg-offset-1 col-xs-12" data-ride="carousel" data-interval="5000">
                            <!-- Indicators -->
                            <ol class="carousel-indicators carousel-indicators--dark">
                                 <?php 
                                if (have_rows('testimonial')):
                                    $j = 0;
                                    while (have_rows('testimonial')):
                                        the_row();
                                     
                            ?>
                                <li data-target="#carousel-quotes" data-slide-to="<?php echo $j;?>" class="<?php if($j==0){?>active<?php }?>"></li>
                                  <?php $j++;
                                        endwhile;                       
                                    endif; 
                                  ?>
                            </ol>

                            <!-- Wrapper for slides -->
                            
                            <div class="carousel-inner" role="listbox">
                                                
                            <?php 
                                if (have_rows('testimonial')):
                                    $i = 1;
                                    while (have_rows('testimonial')):
                                        the_row();
                                        $testimonials_content = get_sub_field('content');
                                        $testimonials_position = get_sub_field('position');
                                        $testimonials_companyname = get_sub_field('company_name');
                            ?>
                                    <div class="item <?php if ($i == 1) { ?>active<?php } ?>">
                                        <h1>“ <?php echo $testimonials_content; ?> ”</h1>
                                            <div class="carousel-caption">
                                                <p><?php echo $testimonials_position; ?>, <?php echo $testimonials_companyname; ?> </p>
                                            </div>
                                    </div>
                                  <?php $i++;
                                        endwhile;                       
                                    endif; 
                                  ?>
     
                            </div>
                        </div>
                    </div>
                   
                   <!--End Testimonials Dynamic-->
                   
                </div>
            </div>
                    
                    <?php if (rwmb_meta('info_graphic')): ?>
                             <div class="container-fluid background-black hidden-xs">
                                <div class="container awards-block">
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6 col-md-offset-3">
                                            <a href="onshore-vs-offshore">
                                                <img srcset="<?php $image = rwmb_meta('info_graphic'); echo reset($image) ['srcset']; ?>">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <?php
                        endif; ?>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <!--Start our Experts Design and dynamic-->
            
            <div class="container-fluid background-white hidden-xs">
                <div class="container expert-block">
                    <h1 class="homepage-section-header">Our Experts</h1>
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                        <?php
                            $args = array('post_type' => 'people', 
                                          'orderby' => 'meta_value_num', 
                                          'meta_key' => 'person_order', 
                                          'order' => 'ASC');
                            $the_query = new WP_Query($args);
                            
                            if ($the_query->have_posts()): 
                                
                                while ($the_query->have_posts()):
                                    $the_query->the_post(); ?>        
                            
                                        <div class="swiper-slide">
                                            <div class="expertslide">
                                                <img srcset="<?php $image = rwmb_meta('person_head_shot'); echo reset($image) ['srcset']; ?>">
                                                <h3><?php echo rwmb_meta('person_name'); ?></h3>
                                                <h5><?php echo rwmb_meta('person_title'); ?></h5>
                                            </div>
                                        </div>
                        <?php
                                endwhile; 
                            endif; 
                            wp_reset_postdata(); 
                        ?>
                        </div>
                        
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
            
            <!--End our Experts Design and dynamic-->
            
        </div>
    </main>
    <script>
        (function ($) {
            $(".desktop-slider__indicator").hide();
            $().ready(function () {
                      setTimeout(function(){ 
        if($(".desktop-slider__indicator").find('.active').length > 0){
            $(".desktop-slider__indicator").show();
        }else{
            $(".desktop-slider__indicator").hide();
        }

     }, 500);
                $.initHomePage();
            });

            AOS.init();
            
        })(jQuery);
    </script>
    
<?php
get_footer();
