<?php
/* Template Name: Services */

get_header(); 


/* Start Services data dynamic call from here */

$services_args = array(
    'post_type' => 'services',
    'order_by' => 'id',
    'order'   => 'ASC'
);
$services_list_loop = new WP_Query( $services_args );

/* End Services data dynamic call from here */                            
?>

    <main class="services">
        
        <!--Start Top Section Dynamic-->
        
        <div class="container-fluid background-black about-title-block">
            <div class="container">
                <div class="about-people-header row">
                    <div class="col-sm-8">
                        <h1>
                            <span class="light-blue">
                                <?php echo get_field('top_section_blue_title')?>
                            </span>
                            <?php echo get_field('top_section_black_title')?>
                        </h1>
                        <div class="home-title__arrow bounce visible-xs-block">
                            <button class="scroll-to-slider--js">
                                <i class="arrow-down" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--End Top Section Dynamic-->
        
        <div class="container background-white">

            <h1 class="homepage-section-header">Our Areas of Expertise</h1>
            <h3 class="homepage-section-header-span">The foundation</h3>
            
            <div class="row expertise-info">
                  <?php 
                    if ( $services_list_loop->have_posts() ) :
                        $mob_web_service = 1; 
                        while ( $services_list_loop->have_posts() ) : $services_list_loop->the_post();
                            if($mob_web_service == '1'):
                                $services_title = strtolower(get_the_title());
                                $services_tab = str_replace(' ','',$services_title);

                        ?>
                     
                <div class="col-sm-12 col-md-7 col-lg-6">
                    <div class="expertise-info--head">
                        <img class="services-icon-img" src="<?php echo get_field('icon');?>">
                            <div>
                                <h5><?php echo get_the_title();?></h5>
                                <span>&gt; <?php echo get_field('tag_line');?></span>
                            </div>
                    </div>
                        <?php echo the_content();?>
                </div>
                <?php 
                    endif; 
                    $mob_web_service ++;
                    endwhile; 
                    endif; 
                    wp_reset_postdata(); 
                ?>

                <div class="col-sm-12 col-md-5 col-lg-6">
                    <div class="hidden-sm">
                        <div class="card card--services expertise-info--card">
                            <?php 
                                    if (have_rows('technology')):
                                        while (have_rows('technology')):
                                            the_row();
                                           
                                ?>
                            <div class="card--services--expertise">
                                <h5><?php echo get_sub_field('technology_title');?></h5>
                                <?php if (have_rows('icons')):
                                       
                                        while (have_rows('icons')):
                                            the_row();
                                      ?> 
                                <img src="<?php echo get_sub_field('technology_icons'); ?>" class="tech-logo">
                                <?php
                                           
                                        endwhile;
                                    endif;
                                ?>
                            </div>
                            <?php
                                           
                                        endwhile;
                                    endif; 
                                ?>
                            
                        </div>
                    </div>
                    <div class="visible-sm-block">
                        <div class="card card--services expertise-info--card">
                            
							
							<div class="row">
								<?php 
                                    if (have_rows('technology')):
                                        while (have_rows('technology')):
                                            the_row();
                                           
                                ?>
									<div class='col-sm-6'>
										<div class="card--services--expertise">
											<h5><?php echo get_sub_field('technology_title');?></h5>
											<?php if (have_rows('icons')):
                                       
                                        while (have_rows('icons')):
                                            the_row();
                                      ?> 
											<img src="<?php echo get_sub_field('technology_icons'); ?>" class="tech-logo">
											 <?php
                                           
                                        endwhile;
                                    endif;
                                ?>
										</div>
									</div>
									
									<?php
                                           
                                        endwhile;
                                    endif; 
                                ?>
									
							</div>
							<!--<div>   
                                <div class="card--services--expertise">
                                    <h5>iOS</h5>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/swift-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/apple-logo.png" class="tech-logo">
                                </div>
                                <div class="card--services--expertise">
                                    <h5>Android</h5>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/kotlin-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/java-logo.png" class="tech-logo">
                                </div>
                            </div>
                            <div>
                                <div class="card--services--expertise">
                                    <h5>Hybrid</h5>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/angular-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/react-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/js-logo.png" class="tech-logo">
                                </div>
                                <div class="card--services--expertise">
                                    <h5>Web</h5>
                                    <img src="<?php bloginfo('template_url'); ?>/assets/magento-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/shopify-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/microsoft-dynamics-logo.png" class="tech-logo">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/apn-logo.png" class="tech-logo">
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            

            <h3 class="homepage-section-header-span">The technology</h3>

            <div class="hidden-sm hidden-xs">
                <div class="servicestab">

                    <div class="tab">
                        <?php
                            if ( $services_list_loop->have_posts() ) :
                                $iservices = 1; 
                                 while ( $services_list_loop->have_posts() ) : $services_list_loop->the_post();
                                if($iservices != '1'):
                                $services_title = strtolower(get_the_title());
                                $services_tab = str_replace(' ','',$services_title);
                        ?>
                            <button class="tablinks" onclick="openTab(event, '<?php echo $services_tab;?>')" id="<?php if($iservices == '2'){ echo 'defaultOpen';}?>"><?php echo the_title();?></button>
                        <?php 
                            endif; 
                                $iservices ++;
                                endwhile; 
                              endif; 
                            wp_reset_postdata(); 
                        ?>

                    </div>
                    <?php
                            if ( $services_list_loop->have_posts() ) :
                                $iservices = 1; 
                                 while ( $services_list_loop->have_posts() ) : $services_list_loop->the_post();
                                if($iservices != '1'):
                                $services_title = strtolower(get_the_title());
                                $services_tab = str_replace(' ','',$services_title);
                        
                    ?>    
                    <div id="<?php echo $services_tab;?>" class="tabcontent tabcontent-md">
                      <div class="tabcontent--head">
                        <img src="<?php echo get_field('icon')?>">
                          <h3> > <?php echo get_field('tag_line');?></h3>
                      </div>
                        <?php the_content();?>
                        <?php if(get_field('view_case_study_url')){ ?>
                        <a href="#">View case study <img src="<?php bloginfo('template_url'); ?>/assets/icons/icon-arrow.svg"></a>
                        <?php }
                        if(get_field('download_whitepaper')){
                        ?>
                        <a href="#">Download whitepaper <img src="<?php bloginfo('template_url'); ?>/assets/icons/icon-arrow.svg"></a>
                        <?php }?>
                    </div>

                   <?php 
                            endif; 
                            $iservices ++;
                            endwhile; 
                            endif; 
                            wp_reset_postdata(); 
                        ?>
                   
                </div>
            </div>

            <div class="visible-sm-block hidden-xs">
                <div class="servicestab">
                    <?php
                            if ( $services_list_loop->have_posts() ) :
                                $iservices = 1; 
                                 while ( $services_list_loop->have_posts() ) : $services_list_loop->the_post();
                                if($iservices != '1'):
                                $services_title = strtolower(get_the_title());
                                $services_tab = str_replace(' ','',$services_title);
                                
                        
                    ?>    
                    <div class="tabcontent">
                      <div class="tabcontent--head">
                        <img src="<?php echo get_field('icon')?>">
                        <div class="tabcontent--head--sm">
                            <h2><?php the_title();?></h2>
                            <h3> > <?php echo get_field('tag_line');?></h3>
                        </div>
                      </div>
                        <?php the_content();?>
                        <?php if(get_field('view_case_study_url')){ ?>
                        <a href="#">View case study <img src="<?php bloginfo('template_url'); ?>/assets/icons/icon-arrow.svg"></a>
                        <?php }
                        if(get_field('download_whitepaper')){
                        ?>
                        <a href="#">Download whitepaper <img src="<?php bloginfo('template_url'); ?>/assets/icons/icon-arrow.svg"></a>
                        <?php }?>
                    </div>

                   <?php 
                            endif; 
                            $iservices ++;
                            endwhile; 
                            endif; 
                            wp_reset_postdata(); 
                        ?>
                </div>
            </div>

            <div class="visible-xs-block">
                <div class="servicestab">
                    <div class="swiper-container serviceswiper">
                        <div class="swiper-wrapper">
                            <?php
                                    if ( $services_list_loop->have_posts() ) :
                                        $iservices = 1; 
                                         while ( $services_list_loop->have_posts() ) : $services_list_loop->the_post();
                                        if($iservices != '1'):
                                        $services_title = strtolower(get_the_title());
                                        $services_tab = str_replace(' ','',$services_title);
                                        
                                
                            ?>
                            <div class="swiper-slide">
                                <div class="tabcontent">
                                  <div class="tabcontent--head">
                                    <img src="<?php echo get_field('icon')?>">
                                    <div class="tabcontent--head--sm">
                                        <h2><?php the_title();?></h2>
                                        <h3> > <?php echo get_field('tag_line');?></h3>
                                    </div>
                                  </div>
                                    <?php the_content();?>
                                    <?php if(get_field('view_case_study_url')){ ?>
                                    <a href="#">View case study <img src="<?php bloginfo('template_url'); ?>/assets/icons/icon-arrow.svg"></a>
                                    <?php }
                                    if(get_field('download_whitepaper')){
                                    ?>
                                    <a href="#">Download whitepaper <img src="<?php bloginfo('template_url'); ?>/assets/icons/icon-arrow.svg"></a>
                                    <?php }?>
                                </div>
                            </div>

                           <?php 
                                    endif; 
                                    $iservices ++;
                                    endwhile; 
                                    endif; 
                                    wp_reset_postdata(); 
                                ?>
                            
                        </div>
                        <div class="swiper-pagination"></div>
                    </div>
                </div>
            </div>

            <h1 class="homepage-section-header">Our Process</h1>
            
            <div class="process-block">
                <?php 
                    if (have_rows('services_process')):
                        $i = 1;
                        while (have_rows('services_process')):the_row();
                ?>
                <div class="row no-gutters display-flex fade-in-bottom--js">
                    <div class="col-xs-12 col-md-6 <?php if($i%2==0){ echo 'col-md-push-6';}?>">
                        <div class="process-block__circle-container">
                            <i class="circle-blue"></i>
                        </div>
                        <div class="process-block__item process-block__slide mobile-slide--js">
                            <h5><?php echo get_sub_field('title');?></h5>
                            <p class="long-description"><?php echo get_sub_field('description');?></p>
                            
                            <?php 
                    if (have_rows('process_sub_data')):
                        $j = 1;
                        while (have_rows('process_sub_data')):the_row();
                ?>            
                            <div class="process-block__single-icon-container">
                                <div class="process-block__icon-container">
                                    <i class="process-icon process-bars" style="background: url('<?php echo get_sub_field('icon'); ?>') center no-repeat !important;"></i>
                                </div>
                                <div class="process-block__icon-description-container">
                                    <p class="process-block__description-title"><?php echo get_sub_field('sub_title');?></p>
                                    <p class="process-block__description"><?php echo get_sub_field('detail');?></p>
                                </div>
                            </div>
                            <?php
                        $j++;
                        endwhile;
                    endif; 
                ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6  <?php if($i%2==0){ echo 'col-md-pull-6 process-block__image-col';}?>">
                        <div class="process-block__item process-block__image-container">
                            <img src="<?php echo get_sub_field('image');?>">
                        </div>
                    </div>
                </div>

                <?php
                        $i++;
                        endwhile;
                    endif; 
                ?>
                
            </div>
        </div>
        <div class="container container--challenges background-white">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <h1 class="homepage-section-header">Clients come to us with a variety of challenges</h1>
                </div>
            </div>
            <div class="row challenge-block challenge-block--even fade-in-bottom--js">
                <?php $hrefCaseStudy = rwmb_meta('challenge_page_href_0');
                ?>
                <div class="col-md-6 challenge-block__info-col">
                    <div class="challenge-block__info">
                        <div>
                            <div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1">
                                <h5><?php echo rwmb_meta('challenge_header_0'); ?></h5>
                                <p><?php echo rwmb_meta('challenge_copy_0'); ?></p>
                                <a class="btn btn--grey" href="<?php echo $hrefCaseStudy?>">View Scotts Miracle-Gro Case Study</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 challenge-block__image-col">
                    <div class="challenge-block__image-container">
                        <div class="challenge-block__image-overlay visible-xs-block"></div>
                        <img srcset="<?php
                        $image = rwmb_meta('challenge_image_0');
                        echo reset($image)['srcset']; ?>">
                        <!-- <a class="btn btn--white visible-xs-inline" href="<?php echo $hrefCaseStudy?>">VIEW CASE STUDY</a>? -->
                        <div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row challenge-block challenge-block--odd fade-in-bottom--js">
                <?php $hrefCaseStudy = rwmb_meta('challenge_page_href_1');?>
                <div class="col-md-6 challenge-block__info-col flex-order-2">
                    <div class="challenge-block__info ">
                        <div>
                            <div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1">
                                <h5><?php echo rwmb_meta('challenge_header_1'); ?></h5>
                                <p><?php echo rwmb_meta('challenge_copy_1'); ?></p>
                                <a class="btn btn--grey" href="<?php echo $hrefCaseStudy?>">View Simple Human Case Study</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 flex-order-1 challenge-block__image-col">
                    <div class="challenge-block__image-container ">
                        <div class="challenge-block__image-overlay visible-xs-block"></div>
                        <img srcset="<?php
                        $image = rwmb_meta('challenge_image_1');
                        echo reset($image)['srcset']; ?>">
                        <!-- <a class="btn btn--white visible-xs-inline" href="<?php echo $hrefCaseStudy?>">VIEW CASE STUDY</a> -->
                    </div>
                </div>
            </div>

            <div class="row challenge-block challenge-block--even fade-in-bottom--js">
                <?php $hrefCaseStudy = rwmb_meta('challenge_page_href_2');?>
                <div class="col-md-6 challenge-block__info-col">
                    <div class="challenge-block__info">
                        <div>
                            <div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1">
                                <h5><?php echo rwmb_meta('challenge_header_2'); ?></h5>
                                <p><?php echo rwmb_meta('challenge_copy_2'); ?></p>
                                <a class="btn btn--grey" href="<?php echo $hrefCaseStudy?>">View Stanford Medicine Case Study</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 challenge-block__image-col">
                    <div class="challenge-block__image-container">
                        <div class="challenge-block__image-overlay visible-xs-block"></div>
                        <img srcset="<?php
                        $image = rwmb_meta('challenge_image_2');
                        echo reset($image)['srcset']; ?>">
                        <!-- <a class="btn btn--white visible-xs-inline" href="<?php echo $hrefCaseStudy?>">VIEW CASE STUDY</a> -->
                        <div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row challenge-block challenge-block--odd fade-in-bottom--js">
                <?php $hrefCaseStudy = rwmb_meta('challenge_page_href_3');?>
                <div class="col-md-6 challenge-block__info-col flex-order-2">
                    <div class="challenge-block__info ">
                        <div>
                            <div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1">
                                <h5><?php echo rwmb_meta('challenge_header_3'); ?></h5>
                                <p><?php echo rwmb_meta('challenge_copy_3'); ?></p>
                                <a class="btn btn--grey" href="<?php echo $hrefCaseStudy?>">View ACTually Case Study</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 flex-order-1 challenge-block__image-col">
                    <div class="challenge-block__image-container ">
                        <div class="challenge-block__image-overlay visible-xs-block"></div>
                        <img srcset="<?php
                        $image = rwmb_meta('challenge_image_3');
                        echo reset($image)['srcset']; ?>">
                        <!-- <a class="btn btn--white visible-xs-inline" href="<?php echo $hrefCaseStudy?>">VIEW CASE STUDY</a> -->
                    </div>
                </div>
            </div>

            <h1 class="lastsec-header">What is your challenge?</h1>
    </main>
    <script>
        (function ($) {
            $().ready(function () {
                $.initServices();
            });
        })(jQuery);

        function openTab(evt, tabName) {
          var i, tabcontent, tablinks;
          tabcontent = document.getElementsByClassName("tabcontent-md");
          for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
          }
          tablinks = document.getElementsByClassName("tablinks");
          for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
          }
          document.getElementById(tabName).style.display = "block";
          evt.currentTarget.className += " active";
        }

        document.getElementById("defaultOpen").click();
    </script>

<?php
get_footer();
