<?php

get_header(); ?>
    <main>
            <div class="container">
                <div class="about-people-header row">
                    <div class="col-md-8">
                        <h1>Meet our extraordinary team</h1>
                    </div>
                </div>
                <div class="category-list">
                    <a href="<?php echo get_post_type_archive_link('people'); ?>">All</a>
                    <?php
                    $terms = get_terms([
                        'post_type' => 'job_type',
                    ]);

                    $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                    foreach ( $terms as $category):
                        echo '<a href="' . get_category_link($category->term_id) . '" class=' . ($term->term_id == $category->term_id ? 'active' : '') . '>' . $category->name . '</a>';
                    endforeach; ?>
                </div>

            <?php
            $args = array(
                'post_type' => 'people',
                'orderby' => 'meta_value_num',
                'meta_key' => 'person_order_landing',
                'posts_per_page' => 1000,
                'order' => 'ASC',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'job_type',
                        'terms'    => $term->term_id,
                    ),
                ));
            $the_query = new WP_Query($args); ?>
            <?php if ($the_query->have_posts()) : ?>
                <div class="row person-list">
                    <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                        <div class="col-sm-4 person-block">
                            <div class="person-block__content">
                                <div class="person-block__image-container">
                                    <div class="person-block__quote"><?php echo rwmb_meta('person_quote') ?></div>
                                    <img srcset="<?php
                                    $image = rwmb_meta('person_head_shot');
                                    echo reset($image)['srcset']; ?>">
                                </div>
                                <div class="person-block__description">
                                    <h5><?php echo rwmb_meta('person_name'); ?></h5>
                                    <p><?php echo rwmb_meta('person_title'); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>
        </div>
    </main>
<?php
get_footer();
