Wordpress theme for swensonshe website.

<br>This project uses webpack. To get started go into the wp-content/themes/swensonhe directory and run
<br>
\>\> npm install
<br>
\>\> webpack --watch
<br>

To start install wordpress https://codex.wordpress.org/Installing_WordPress
and import this theme in the admin